Speedy
====
[![pipeline status](https://gitlab.com/AngryTester/speedy/badges/master/pipeline.svg)](https://gitlab.com/AngryTester/speedy/commits/master)

## 一. 项目摘要

Speedy是一个支持并发UI自动化测试的框架，基于开源的Selenium WebDriver,Grid,TestNG,ReportNG二次封装，结合容器化技术可以快捷地实现低成本的多并发UI自动化测试。

## 二. 项目编译

> 编译环境要求

- JDK

JDK版本>1.8，可以通过命令java -version查看：

```shell
$ java -version
java version "1.8.0_77"
Java(TM) SE Runtime Environment (build 1.8.0_77-b03)
Java HotSpot(TM) 64-Bit Server VM (build 25.77-b03, mixed mode)
```

- MAVEN
MAVEN版本>3.2.0，可以通过命令mvn -v查看：

```shell
$ mvn -v
Apache Maven 3.2.5 (12a6b3acb947671f09b81f49094c53f426d8cea1; 2014-12-15T01:29:23+08:00)
Maven home: /opt/apache-maven-3.2.5
Java version: 1.8.0_77, vendor: Oracle Corporation
Java home: /opt/jdk1.8.0_77/jre
Default locale: zh_CN, platform encoding: UTF-8
OS name: "linux", version: "4.13.0-43-generic", arch: "amd64", family: "unix"
```

### 编译

```
mvn compile
```

### 单元测试

```
mvn test
```

## 三. 代码示例

> 浏览器操作：

```java
public void openBrowserTest(){
	Browser browser = ChromeBrowser.start("https://gitlab.com/");
	browser.element("id=>user_login").sendKeys("thx_phila@yahoo.com");
	browser.element("id=>user_password").sendKeys("123456");
	browser.element("name=>commit").click();
	browser.quit();
}
```

> 加入断言：

```java
public void openBrowserTest(){
	Browser browser = ChromeBrowser.start("https://gitlab.com/");
	browser.element("id=>user_login").sendKeys("thx_phila@yahoo.com");
	browser.element("id=>user_password").sendKeys("123456");
	browser.element("name=>commit").click();
	Asserter asserter = new Asserter(browser);
	asserter.assertEquals(browser.link("text=>New Project"), true,"new project按钮存在");
	Asserter.assertAll();
	browser.quit();
}
```
> testng.xml示例：

```xml
<!DOCTYPE suite SYSTEM "http://testng.org/testng-1.0.dtd" >
<suite name="speedy" parallel="tests" thread-count="8"  data-provider-thread-count="8">
	<test name="login">
		<packages>
			<package name="com.angrytest.autotest.login" />
		</packages>
	</test>
</suite>
```

> pom.xml示例：

```xml
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>com.angrytest.autotest</groupId>
	<artifactId>autotest</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<packaging>jar</packaging>
	<name>autotest</name>
	<description>自动化测试</description>
	<dependencies>
		<dependency>
			<groupId>com.angrytest</groupId>
			<artifactId>speedy</artifactId>
			<version>1.0.0-SNAPSHOT</version>
		</dependency>
	</dependencies>
	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.6.1</version>
				<configuration>
					<source>1.8</source>
					<target>1.8</target>
					<encoding>UTF-8</encoding>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>2.4.2</version>
				<configuration>
					<suiteXmlFiles>
						<file>testng.xml</file>
					</suiteXmlFiles>
					<argLine>-Dfile.encoding=UTF-8</argLine>
					<properties>
						<property>
							<name>usedefaultlisteners</name>
							<value>false</value>
						</property>
						<property>
							<name>listener</name>
							<value>com.angrytest.reportng.HTMLReporter</value>
						</property>
					</properties>
				</configuration>
			</plugin>
		</plugins>
	</build>
</project>
```

====

TO DO...