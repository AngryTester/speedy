package com.angrytest.locators;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.angrytest.browsers.Browser;
import com.angrytest.containers.Container;
import com.angrytest.elements.Element;

/**
 * 元素定位
 *
 * @ClassName: ElementLocator
 * @Description: 元素定位
 * @author AngryTester
 * @date 2017年5月15日 上午11:22:42
 *
 */
public class ElementLocator {
	public String id;
	public String name;
	public String xpath;
	public String className;
	public WebElement element;
	public WebDriver driver;
	public Container container;
	public HashMap<String, Object> options;
	public HashMap<String, Object> regexpOptions = new HashMap<String, Object>();
	public HashMap<String, Object> otherOptions = new HashMap<String, Object>();
	public static By by;

	@SuppressWarnings("unchecked")
	public ElementLocator(Container container, HashMap<String, Object> options) {
		this.container = container;
		this.options = (HashMap<String, Object>) options.clone();
	}

	public ElementLocator() {

	}

	@SuppressWarnings("unused")
	private Boolean checkElementProperty() {
		return true;
	}

	@SuppressWarnings("rawtypes")
	public WebElement locate() {
		// 判断是否有正则，有的话需要去掉正则，另外有index的话，也要先去掉
		otherOptions = (HashMap<String, Object>) getRegexpOptions(options);
		if (regexpOptions.size() > 0) {
			// 走正则
			List<WebElement> webElementList = new ArrayList<WebElement>();
			int index = 1;
			if (otherOptions.containsKey("index")) {
				index = Integer.parseInt(otherOptions.remove("index").toString());
			}
			if (otherOptions.size() == 0)
				webElementList = allElements();
			else if (otherOptions.size() == 1) {
				Iterator iter = otherOptions.entrySet().iterator();
				while (iter.hasNext()) {
					Map.Entry entry = (Map.Entry) iter.next();
					String key = (String) entry.getKey();
					String value = (String) entry.getValue();
					if (value.indexOf("|") > 0) {
						webElementList = findAllByMultiple(otherOptions);
					} else {
						webElementList = findAllByOne(key, value);
					}
				}
			} else {
				webElementList = findAllByMultiple(otherOptions);
			}
			// webElementList正则匹配后的结果
			// element = matchedElementList(webElementList).get(index);
			element = matchedElement(webElementList, index);
		} else {
			// 走基本流
			if (options.size() == 1) {
				Iterator iter = options.entrySet().iterator();
				while (iter.hasNext()) {
					Map.Entry entry = (Map.Entry) iter.next();
					String key = (String) entry.getKey();
					String value = (String) entry.getValue();
					if (key.contentEquals("index")) {
						element = allElements().get(Integer.parseInt(value) - 1);
					} else {
						element = findFirstByOne(key, value);
					}
				}

			} else {
				element = findFirstByMultiple(options);
			}
		}

		return element;
	}

	@SuppressWarnings("rawtypes")
	private WebElement matchedElement(List<WebElement> webElementList, int index) {
		WebElement matchedElement = null;
		List<WebElement> matchedElementList = new ArrayList<WebElement>();
		for (WebElement webElement : webElementList) {
			Boolean matched = false;
			Iterator iter = regexpOptions.entrySet().iterator();
			while (iter.hasNext()) {
				Map.Entry entry = (Map.Entry) iter.next();
				String key = (String) entry.getKey();
				Pattern value = (Pattern) entry.getValue();
				// class
				String actualValue = "";
				if (key.equals("text")) {
					actualValue = webElement.getText();

				} else if (key.equals("suffixText")) {
					JavascriptExecutor js = (JavascriptExecutor) container.driver;
					actualValue = (String) js
							.executeScript(
									"var nextSibling = arguments[0].nextSibling; while(nextSibling && nextSibling.nodeType != 3){nextSibling = nextSibling.nextSibling;}return nextSibling.nodeValue;",
									webElement);
				} else if (key.equals("prefixText")) {
					JavascriptExecutor js = (JavascriptExecutor) container.driver;
					actualValue = (String) js
							.executeScript(
									"var previousSibling = arguments[0].previousSibling; while(previousSibling && previousSibling.nodeType != 3){previousSibling = previousSibling.previousSibling;}return previousSibling.nodeValue;",
									webElement);
				} else {
					actualValue = webElement.getAttribute(key);
					// System.out.println(actualValue);
				}
				if (actualValue == null) {
					continue;
				}
				Matcher matcher = value.matcher(actualValue);
				if (matcher.matches() || matcher.find()) {
					matched = true;
				} else {
					matched = false;
					break;
				}
			}
			if (matched) {
				if (matchedElementList.size() == index - 1) {
					matchedElement = webElement;
					break;
				}
				matchedElementList.add(webElement);
			}
		}
		return matchedElement;
	}

	@SuppressWarnings({ "rawtypes", "unused" })
	private List<WebElement> matchedElementList(List<WebElement> webElementList) {
		List<WebElement> matchedElementList = new ArrayList<WebElement>();
		for (WebElement webElement : webElementList) {
			Boolean matched = false;
			Iterator iter = regexpOptions.entrySet().iterator();
			while (iter.hasNext()) {
				Map.Entry entry = (Map.Entry) iter.next();
				String key = (String) entry.getKey();
				Pattern value = (Pattern) entry.getValue();
				// class
				Matcher matcher = value.matcher(webElement.getAttribute(key));
				if (matcher.matches()) {
					matched = true;
				} else {
					matched = false;
					break;
				}
			}
			if (matched) {
				matchedElementList.add(webElement);
			}
		}
		return matchedElementList;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Map<String, Object> getRegexpOptions(HashMap<String, Object> selectors) {
		// Map<String, Object> otherOptions = new HashMap<String, Object>();
		// Map<String, Object> regexpOptions = new HashMap<String, Object>();
		regexpOptions.clear();
		otherOptions.clear();
		Iterator iter = selectors.entrySet().iterator();
		while (iter.hasNext()) {
			Map.Entry<String, Object> entry = (Map.Entry<String, Object>) iter.next();
			if (entry.getValue() instanceof Pattern) {
				regexpOptions.put(entry.getKey(), entry.getValue());
				// options.remove(entry.getKey());// 此方法不行的话，另外再处理
			} else {
				otherOptions.put(entry.getKey(), entry.getValue());
			}
		}
		// options = (HashMap<String, Object>) otherOptions;
		return otherOptions;
	}

	/**
	 * 构建xpath路径，用于控件定位
	 * 
	 * @param options
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public String buildXpath(HashMap<String, Object> options) {
		HashMap<String, Object> selectors = (HashMap<String, Object>) options.clone();
		if (selectors.get("xpath") != null) {
			return selectors.get("xpath").toString();
		}
		String xpath = "";
		Object tagName = selectors.remove("tagName");
		if (tagName == null) {
			xpath = ".//*";

			if (selectors.size() >= 1) {
				xpath += "[";
				Iterator iter = selectors.entrySet().iterator();
				int n = 0;

				while (iter.hasNext()) {

					if (n > 0) {
						xpath += " and ";
					}
					Map.Entry entry = (Map.Entry) iter.next();
					String key = (String) entry.getKey();
					Object value = entry.getValue();
					xpath += equalPair(key, value);
					n += 1;

				}

				// } else {
				// xpath += "*";
			}
			xpath += "]";

		} else {
			String[] tagNames = tagName.toString().split("\\|");
			for (String tag : tagNames) {
				if (xpath.length() > 0) {
					xpath += "|";
				}
				xpath += ".//" + tag;

				if (selectors.size() >= 1) {
					Iterator iter = selectors.entrySet().iterator();
					// while (iter.hasNext()) {
					// xpath += "[";
					// Map.Entry entry = (Map.Entry) iter.next();
					// String key = (String) entry.getKey();
					// Object value = entry.getValue();
					// xpath += equalPair(key, value);
					// xpath += "]";
					// }
					xpath += "[";
					while (iter.hasNext()) {
						if (!xpath.endsWith("[")) {
							xpath += " and ";
						}
						Map.Entry entry = (Map.Entry) iter.next();
						String key = (String) entry.getKey();
						Object value = entry.getValue();
						xpath += equalPair(key, value);

					}
					xpath += "]";
				}
				// System.out.println(xpath);
			}
		}

		return xpath;
	}

	@SuppressWarnings("unused")
	private String formate(String key) {
		String formateKey = "";
		if (key.equals("text")) {
			formateKey = "normalize-space()";
		} else if (key.equals("href")) {
			formateKey = "normalize-space(@href)";
		} else {
			formateKey = "@" + key.replace('_', '-');
		}
		return formateKey;
	}

	private String equalPair(String key, Object value) {
		String pair = "";

		if (key.equals("className") || key.equals("class")) {
			// pair = "contains(concat(' ', @class, ' ')," + value.toString() +
			// ")";
			pair = " @class=" + "'" + value.toString() + "'";
		} else if (key.equals("text")) {
			pair = "normalize-space()" + "='" + value.toString() + "'";
		} else if ((key.equals("suffixText"))) {
			pair = "contains(following-sibling::text()[1],'" + value.toString() + "')";
		} else if ((key.equals("prefixText"))) {
			pair = "contains(preceding-sibling::text()[1],'" + value.toString() + "')";
		} else {
			// if (value != null &&
			// (value.toString().toLowerCase().equals("true") ||
			// value.toString().toLowerCase().equals("false"))) {
			// pair = "@" + key + "=" +
			// Boolean.valueOf(value.toString().toLowerCase());
			// } else {
			pair = "@" + key + "='" + value.toString() + "'";
			// }
		}
		return pair;
	}

	public WebElement byId() {
		element = container.driver.findElement(By.id((String) options.get(id)));
		return element;
	}

	/**
	 * 查找多个属性值的控件
	 * 
	 * @param options
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public WebElement findFirstByMultiple(HashMap<String, Object> options) {
		HashMap<String, Object> multiple = (HashMap<String, Object>) options.clone();
		Object idx = multiple.remove("index");
		String xpath = buildXpath(multiple);
		if (idx == null)
			element = findElement(By.xpath(xpath));
		else {
			element = findElements(By.xpath(xpath)).get(Integer.parseInt(idx.toString()) - 1);
		}
		return element;
	}

	/**
	 * 查找多个属性值的控件列表
	 * 
	 * @param options
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<WebElement> findAllByMultiple(HashMap<String, Object> options) {
		HashMap<String, Object> multiple = (HashMap<String, Object>) options.clone();
		List<WebElement> elementList = new ArrayList<WebElement>();
		String xpath = buildXpath(multiple);
		elementList = findElements(By.xpath(xpath));
		return elementList;
	}

	/**
	 * 查找单个属性值的控件
	 * 
	 * @param key
	 * @param value
	 * @return
	 */
	public WebElement findFirstByOne(String key, String value) {
		if ("class".equals(key)) {
			key = "className";
		}
		Method byMethod = null;
		try {
			byMethod = By.class.getMethod(key, new Class[] { String.class });
			// byMethod = By.class.getDeclaredMethod(key);
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// 没有对应的方法，就用xpath来查找
			HashMap<String, Object> selectors = new HashMap<String, Object>();
			selectors.put(key, value);
			value = buildXpath(selectors);
			try {
				byMethod = By.class.getMethod("xpath", new Class[] { String.class });
			} catch (SecurityException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (NoSuchMethodException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		try {
			// driver.findElement((By) byMethod.invoke(value));
			findElement((By) byMethod.invoke(By.class, new Object[] { value }));
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return element;
	}

	/**
	 * 查找单个属性值的控件列表
	 * 
	 * @param key
	 * @param value
	 * @return
	 */
	public List<WebElement> findAllByOne(String key, String value) {
		List<WebElement> elementList = new ArrayList<WebElement>();
		if ("class".equals(key)) {
			key = "className";
		}
		Method byMethod = null;
		try {
			byMethod = By.class.getMethod(key, new Class[] { String.class });
			// byMethod = By.class.getDeclaredMethod(key);
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// 没有对应的方法，就用xpath来查找
			HashMap<String, Object> selectors = new HashMap<String, Object>();
			selectors.put(key, value);
			value = buildXpath(selectors);
			try {
				byMethod = By.class.getMethod("xpath", new Class[] { String.class });
			} catch (SecurityException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (NoSuchMethodException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		try {
			// driver.findElement((By) byMethod.invoke(value));
			elementList = findElements((By) byMethod.invoke(By.class, new Object[] { value }));
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return elementList;
	}

	private List<WebElement> allElements() {
		List<WebElement> allElements = new ArrayList<WebElement>();
		allElements = findElements(By.xpath(".//*"));
		return allElements;
	}

	private List<WebElement> findElements(By by) {
		List<WebElement> elements = new ArrayList<WebElement>();
		if (container instanceof Browser)
			elements = container.driver.findElements(by);
		if (container instanceof Element) {
			if (container.isFrame) {
				elements = container.driver.findElements(by);
			} else {
				elements = container.current.findElements(by);
			}
		}
		return elements;
	}

	/**
	 * 根据指定属性值找到控件
	 * 
	 * @param by
	 * @return
	 */
	private WebElement findElement(By by) {
		if (container instanceof Browser)
			element = container.driver.findElement(by);
		if (container instanceof Element)
			if (container.isFrame) {
				element = container.driver.findElement(by);
			} else {
				element = container.current.findElement(by);
			}
		return element;

	}
}
