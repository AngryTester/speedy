package com.angrytest.enums;

/**
 * 运行状态
 */
public enum RunStatus {
	WAITING, RUNNING, COMPLETED
}
