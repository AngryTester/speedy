package com.angrytest.enums;

/**
 * 数据源类型
 */
public enum DataSourceType {
	EXCEL, XML, CSV, DB
}
