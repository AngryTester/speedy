package com.angrytest.enums;

/**
 * 运行结果
 */
public enum RunResult {
	FAIL, PASS, RUNNING, WAITING;
}
