package com.angrytest.enums;

/**
 * 步骤类型
 */
public enum StepType {
	ASSERT, ACTION
}
