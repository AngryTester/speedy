package com.angrytest.enums;

/**
 * 浏览器进程类型
 */
public enum BrowserProcessType {
	iexplore, chrome, firefox
}
