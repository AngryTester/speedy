package com.angrytest.enums;

/**
 * 浏览器类型
 */
public enum BrowserType {
	IE, CHROME, FIREFOX, GHOST
}
