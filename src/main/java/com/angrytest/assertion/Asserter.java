package com.angrytest.assertion;

import java.util.HashMap;
import java.util.Map;

import org.testng.Reporter;
import org.testng.asserts.Assertion;
import org.testng.asserts.IAssert;

import com.angrytest.browsers.Browser;
import com.angrytest.enums.RunResult;
import com.angrytest.enums.StepType;
import com.angrytest.logs.LogModule;
import com.angrytest.reportng.HTMLReporter;


/**
 * 重写断言，断言失败不立即退出
 *
 * @ClassName: Asserter
 * @Description: 自定义断言器
 * @author AngryTester
 * @date 2017年6月20日 下午11:44:42
 *
 */
public class Asserter extends Assertion {

	/**
	 * 是否为UI测试
	 */
	private boolean isUI = false;

	private Browser browser;

	public Asserter() {

	}

	/**
	 * 构造方法
	 * @param browser
	 */
	public Asserter(Browser browser) {
		this.browser = browser;
		this.isUI = true;
	}

	/*
	 * 重写断言，失败不退出
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void executeAssert(IAssert a) {
		try {
			a.doAssert();
			System.out.println("预期=" + a.getExpected() + ",实际=" + a.getActual() + "," + a.getMessage() + "断言成功");
			Reporter.log("预期=" + a.getExpected() + ",实际=" + a.getActual() + "," + a.getMessage() + "断言成功");
		} catch (AssertionError ex) {
			onAssertFailure(a, ex);
			if (!HTMLReporter.m_errors_map.isEmpty()) {
				if (HTMLReporter.m_errors_map.containsKey(Thread.currentThread().getId())) {
					Map<AssertionError, IAssert> m_errors = HTMLReporter.m_errors_map
							.remove(Thread.currentThread().getId());
					m_errors.put(ex, a);
					HTMLReporter.m_errors_map.put(Thread.currentThread().getId(), m_errors);
				} else {
					Map<AssertionError, IAssert> m_errors = new HashMap<AssertionError, IAssert>();
					m_errors.put(ex, a);
					HTMLReporter.m_errors_map.put(Thread.currentThread().getId(), m_errors);
				}
			} else {
				Map<AssertionError, IAssert> m_errors = new HashMap<AssertionError, IAssert>();
				m_errors.put(ex, a);
				HTMLReporter.m_errors_map.put(Thread.currentThread().getId(), m_errors);
			}
			if (this.isUI) {
				LogModule.logStepFail(StepType.ASSERT,
						"预期=" + a.getExpected() + ",实际=" + a.getActual() + "," + a.getMessage() + "断言失败",
						RunResult.FAIL, ex.getMessage(), browser.getDriver());
			} else {
				System.out.println("预期=" + a.getExpected() + ",实际=" + a.getActual() + "," + a.getMessage() + "断言失败,"+ex.toString());
				Reporter.log("预期=" + a.getExpected() + ",实际=" + a.getActual() + "," + a.getMessage() + "断言失败,"+ex.toString());
			}
		}
	}

	/**
	 * 断言失败合计
	 */
	@SuppressWarnings("rawtypes")
	public static void assertAll() {
		if (!HTMLReporter.m_errors_map.isEmpty()) {
			if (HTMLReporter.m_errors_map.containsKey(Thread.currentThread().getId())) {
				StringBuilder sb = new StringBuilder("The following asserts failed:\n");
				boolean first = true;
				for (Map.Entry<AssertionError, IAssert> ae : HTMLReporter.m_errors_map
						.get(Thread.currentThread().getId()).entrySet()) {
					if (first) {
						first = false;
					} else {
						sb.append(", ");
					}
					sb.append(ae.getKey().getMessage());
				}
				throw new AssertionError(sb.toString());
			}
		}
	}

}
