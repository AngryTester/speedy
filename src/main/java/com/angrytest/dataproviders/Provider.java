package com.angrytest.dataproviders;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.testng.annotations.DataProvider;

import com.angrytest.annotations.DataSource;
import com.angrytest.enums.DataSourceType;
import com.angrytest.exceptions.AngryException;
import com.angrytest.utils.Config;
import com.angrytest.utils.ExcelUtil;
import com.angrytest.utils.FileUtil;

/**
 * 数据驱动操作类
 * 
 * @ClassName: Provider
 * @Description: 数据驱动
 * @author AngryTester
 * @date 2017年5月17日 上午11:40:49
 * 
 */
public class Provider {

	/**
	 * 解析excel数据源
	 * 
	 * @Title: excelData
	 * @Description: 解析excel数据源-测试方法有两个参数，第一个参数为请求体，第二个参数为数据源map
	 * @param m
	 * @return
	 */
	@DataProvider(name = "excel", parallel = true)
	public static Object[][] excelData(Method m) {
		List<String> requests = new ArrayList<String>();
		init(m);
		ExcelUtil excelUtil = new ExcelUtil();
		List<Map<String, String>> list = excelUtil.excelDatas(
				"src/test/resources/" + m.getAnnotation(DataSource.class).file(),
				m.getAnnotation(DataSource.class).sheetName());
		// 根据caseRun决定是否执行
		List<Map<String, String>> list2 = new ArrayList<Map<String, String>>();
		for (Map<String, String> map : list) {
			if (map.containsKey("caseRun")) {
				if (map.get("caseRun").toUpperCase().equals("Y")) {
					list2.add(map);
				}
			}
		}
		for (Map<String, String> param : list2) {
			String request = FileUtil.initRequestBody("src/test/resources/" + m.getAnnotation(DataSource.class).temp(),
					param);
			requests.add(request);
		}
		Object[][] obj = new Object[requests.size()][];
		for (int i = 0; i < requests.size(); i++) {
			obj[i] = new Object[] { requests.get(i), list2.get(i) };
		}
		return obj;
	}

	/**
	 * @Title: excelData2
	 * @Description: 解析excel数据源-测试方法只有一个参数，为数据源map
	 * @param m
	 * @return
	 */
	@DataProvider(name = "excel2", parallel = true)
	public static Object[][] excelData2(Method m) {
		init(m);
		ExcelUtil excelUtil = new ExcelUtil();
		List<Map<String, String>> list = excelUtil.excelDatas(
				"src/test/resources/" + m.getAnnotation(DataSource.class).file(),
				m.getAnnotation(DataSource.class).sheetName());
		// 根据caseRun决定是否执行
		List<Map<String, String>> list2 = new ArrayList<Map<String, String>>();
		for (Map<String, String> map : list) {
			if (map.containsKey("caseRun")) {
				if (map.get("caseRun").toUpperCase().equals("Y")) {
					list2.add(map);
				}
			}
		}
		Object[][] obj = new Object[list2.size()][];
		for (int i = 0; i < list2.size(); i++) {
			obj[i] = new Object[] { list2.get(i) };
		}
		return obj;
	}

	/**
	 * 解析xml数据源
	 * 
	 * @Title: xmlData
	 * @Description: 解析xml数据源
	 * @return
	 */
	@DataProvider(name = "xml")
	public static Object[][] xmlData() {
		return null;
	}

	/**
	 * 前置检查
	 * 
	 * @Title: init
	 * @Description: 前置检查注解
	 * @param m
	 */
	private static void init(Method m) {
		DataSource dataSource = m.getAnnotation(DataSource.class);
		if (m.getParameterTypes().length == 2 && dataSource.temp().equals("")) {
			throw new AngryException("请指定请求模板文件");
		}
		if (dataSource.type() != DataSourceType.DB && dataSource.file().equals("")) {
			throw new AngryException("请指定数据驱动源文件");
		}
	}
}
