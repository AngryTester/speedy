package com.angrytest.elements;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.angrytest.containers.Container;


public class Row extends Element {
	public Row(Container container, HashMap<String, Object> options) {
		super(container, options);
	}

	/**
	 * 获取当前行的所有单元格，index值从1开始
	 * 
	 * @return List<Element>，单元格元素集合
	 * @remark index值从1开始
	 * @Example
	 * browser.table("id=>table").row("index=>2").getCellList(); 
	 */
	public List<Element> getCellList() {
		HashMap<String, Object> temp = new HashMap<String, Object>();
		temp.put("tagName", ".//th|.//td");
		temp.put("methodInfo", methodInfo + "getCellList()");
		return getElementList(temp);
	}

	/**
	 * 获取当前行的所有单元格的内容与序号匹配，主要用于表格的标题行;
	 * 该方法主要用于避免因表格增减列数时，导致直接用几行几列来识别单元格的维护成本	
	 * @return Map<String, Integer>，当前行所有的单元格的内容与序号匹配集合
	 * @Example
	 * <div>Map<String, Integer> cellTitleList = cb.table("class=>dataintable").row("index=>1").getCellTextIndexMap();</div>
     * <div>cb.table("class=>dataintable").getCell(2, cellTitleList.get("旅客")).text();</div>
	 */
	public Map<String, Integer> getCellTextIndexMap() {
		Map<String, Integer> cellTextIndexMap = new HashMap<String, Integer>();
		List<String> cellTextList = new ArrayList<String>();
		HashMap<String, Object> temp = new HashMap<String, Object>();
		temp.put("tagName", ".//th|.//td");
		temp.put("methodInfo", methodInfo + ".getCellTextIndexMap()");
		cellTextList = getElementTextList(temp);
		for (int i = 0; i < cellTextList.size(); i++) {
			String text = cellTextList.get(i);
			cellTextIndexMap.put(text, i + 1);
		}
		return cellTextIndexMap;
	}

	/**
	 * 获取当前行的所有单元格的内容,index值从1开始
	 * @return List<String>，所有单元格内容集合
	 * @remark index值从1开始
	 * @Example
	 * browser.table("id=>table").row("index=>2").getCellTextList(); 
	 */
	public List<String> getCellTextList() {
		List<String> cellTextList = new ArrayList<String>();
		HashMap<String, Object> temp = new HashMap<String, Object>();
		temp.put("tagName", ".//th|.//td");
		temp.put("methodInfo", methodInfo + ".getCellTextList()");
		cellTextList = getElementTextList(temp);
		return cellTextList;
	}
}
