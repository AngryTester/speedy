package com.angrytest.elements;

import java.util.HashMap;
import com.angrytest.containers.Container;
import com.angrytest.enums.RunResult;
import com.angrytest.enums.StepType;
import com.angrytest.exceptions.AngryException;
import com.angrytest.logs.LogModule;

public class Radio extends Element {
	public Radio(Container container, HashMap<String, Object> options) {
		super(container, options);
	}

	/**
	 * 设置单选框选中状态，默认只有选中方法
	 * 
	 * @param bool true|false
	 */
	public void set(Boolean bool) {
		methodInfo += ".set(" + bool + ")";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			highlight(true);
			int retry = 3;
			while (current.isSelected() != bool && retry > 0) {
				current.click();
				retry--;
			}
			highlight(false);
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}
}
