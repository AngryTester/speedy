package com.angrytest.elements;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.angrytest.containers.Container;
import com.angrytest.enums.RunResult;
import com.angrytest.enums.StepType;
import com.angrytest.exceptions.AngryException;
import com.angrytest.logs.LogModule;

public class Select extends Element {
	protected static final Log logger = LogFactory.getLog(Select.class);

	public Select(Container container, HashMap<String, Object> options) {
		super(container, options);
	}

	/**
	 * 根据select中option的value来选择
	 * 
	 * @param value option的value值
	 */
	public void setOptionByValue(String value) {
		methodInfo += ".setOptionByValue(\"" + value + "\")";
		Boolean exists = false;
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			List<WebElement> optionList = current.findElements(By.tagName("option"));
			for (WebElement op : optionList) {
				if (value.equalsIgnoreCase(op.getAttribute("value"))) {
					op.click();
					exists = true;
					break;
				}
			}
			if (!exists) {
				throw new Exception("找不到选项：" + value);
			}
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	/**
	 * 根据select中option的text来选择
	 * 
	 * @param text option的text值
	 */
	public void setOptionByText(String text) {
		methodInfo += ".setOptionByText(\"" + text + "\")";
		Boolean exists = false;
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			List<WebElement> optionList = current.findElements(By.tagName("option"));
			Pattern pattern = null;
			if (text.startsWith("/") && text.endsWith("/")) {
				pattern = Pattern.compile(text.substring(1, text.length() - 1));
			}
			for (WebElement op : optionList) {
				// System.out.println(text);
				// System.out.println(op.getText());
				// System.out.println(text.equalsIgnoreCase(op.getText().trim()));
				String actualText = op.getText();
				if (actualText == null) {
					continue;
				} else if (pattern == null && text.equalsIgnoreCase(actualText.trim())) {
					op.click();
					exists = true;
					break;
				} else if (pattern != null) {
					Matcher matcher = pattern.matcher(actualText.trim());
					if (matcher.matches() || matcher.find()) {
						op.click();
						exists = true;
						break;
					}
				}
			}
			if (!exists) {
				throw new Exception("找不到选项：" + text);
			}
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	/**
	 * 根据select中option的序号来选择
	 * 
	 * @param index int index，option的序号,index从1开始
	 * @Example
	 * browser.select("id=>source").setOptionByIndex(1);
	 */
	public void setOptionByIndex(int index) {
		methodInfo += ".setOptionByIndex(" + index + ")";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			List<WebElement> optionList = current.findElements(By.tagName("option"));
			optionList.get(index - 1).click();
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	/**
	 * 指定select中option在序号区间来随机选择
	 * 
	 * @param start 开始序号
	 * @param end 结束序号
	 * @Example
	 * browser.select("id=>source").setOptionByRand(1,3);
	 */
	public void setOptionByRand(int start, int end) {
		Random random = new Random(start);
		int index = start + random.nextInt(end + 1 - start);
		methodInfo += ".setOptionByRand(" + start + "," + end + ")";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			List<WebElement> optionList = current.findElements(By.tagName("option"));
			optionList.get(index - 1).click();
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	/**
	 * 随机选择select中的option
	 */
	public void setOptionByRand() {
		int start = 1;
		methodInfo += ".setOptionByRand()";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			List<WebElement> optionList = current.findElements(By.tagName("option"));
			Random random = new Random();
			int index = start + random.nextInt(optionList.size() + 1 - start);
			methodInfo += "在" + optionList.size() + "选项中随机到第" + index + "个选项";
			optionList.get(index - 1).click();
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	/**
	 * 获取select中所有选中框的text值列表
	 * @return List<String>,选中了的option的text值
	 * @Example
	 * browser.select("id=>source").getSelectedTextList();
	 */
	public List<String> getSelectedTextList() {
		methodInfo += ".getSelectedTextList()";
		List<String> selectedTextList = new ArrayList<String>();
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			List<WebElement> optionList = current.findElements(By.tagName("option"));
			for (WebElement element : optionList) {
				if (element.isSelected()) {
					selectedTextList.add(element.getText().toString());
				}
			}
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
		return selectedTextList;

	}

	/**
	 * 获取select中所有选中框的value值列表
	 * @return List<String>,选中了的option的value值
	 * @Example
	 * browser.select("id=>source").getSelectedValueList();
	 */
	public List<String> getSelectedValueList() {
		List<String> selectedValueList = new ArrayList<String>();
		methodInfo += ".getSelectedValueList()";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			List<WebElement> optionList = current.findElements(By.tagName("option"));
			for (WebElement element : optionList) {
				if (element.isSelected()) {
					selectedValueList.add(element.getAttribute("value"));
				}
			}
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
		return selectedValueList;
	}

	/**
	 * 获取select中的option选项列表
	 * @return Option集合
	 * @Example
	 * List<Element> list = browser.select("id=>mainDomainSelect").getOptionList();
	 */
	public List<Element> getOptionList() {
		HashMap<String, Object> temp = new HashMap<String, Object>();
		temp.put("tagName", ".//option");
		temp.put("methodInfo", methodInfo + "getOptionList()");
		return getElementList(temp);
	}

	/**
	 * 获取select中的option选项的文本值列表
	 * @return List<String>,option文本值集合
	 * @Example
	 * browser.select("id=>source").getOptionTextList();
	 */
	public List<String> getOptionTextList() {
		List<String> optionTextList = new ArrayList<String>();
		HashMap<String, Object> temp = new HashMap<String, Object>();
		temp.put("tagName", ".//option");
		temp.put("methodInfo", methodInfo + "getOptionTextList()");
		optionTextList = getElementTextList(temp);
		return optionTextList;
	}

}
