package com.angrytest.elements;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.angrytest.containers.Container;
import com.angrytest.enums.RunResult;
import com.angrytest.enums.StepType;
import com.angrytest.exceptions.AngryException;
import com.angrytest.logs.LogModule;

public class Cell extends Element {

	public Cell(Container container, HashMap<String, Object> options) {
		super(container, options);
	}

	public int index() {
		int index = 0;
		methodInfo += ".index()";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			WebElement parent = (WebElement) executeJs("return arguments[0].parentNode;", current);
			List<WebElement> webElementList = parent.findElements(By.xpath("./*"));

			for (WebElement webElement : webElementList) {
				index++;
				if (webElement.getText().equals(current.getText())) {
					break;
				}
			}
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		return index;
	}
}
