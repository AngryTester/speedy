package com.angrytest.elements;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.angrytest.containers.Container;
import com.angrytest.enums.RunResult;
import com.angrytest.enums.StepType;
import com.angrytest.exceptions.AngryException;
import com.angrytest.locators.ElementLocator;
import com.angrytest.logs.LogModule;
import com.angrytest.pagefactory.interfaces.IBody;
import com.angrytest.pagefactory.interfaces.IButton;
import com.angrytest.pagefactory.interfaces.ICell;
import com.angrytest.pagefactory.interfaces.ICheckbox;
import com.angrytest.pagefactory.interfaces.IDiv;
import com.angrytest.pagefactory.interfaces.IElement;
import com.angrytest.pagefactory.interfaces.IFileField;
import com.angrytest.pagefactory.interfaces.IForm;
import com.angrytest.pagefactory.interfaces.IFrame;
import com.angrytest.pagefactory.interfaces.IInput;
import com.angrytest.pagefactory.interfaces.ILink;
import com.angrytest.pagefactory.interfaces.IOption;
import com.angrytest.pagefactory.interfaces.IRadio;
import com.angrytest.pagefactory.interfaces.IRow;
import com.angrytest.pagefactory.interfaces.ISelect;
import com.angrytest.pagefactory.interfaces.ISpan;
import com.angrytest.pagefactory.interfaces.ITable;
import com.angrytest.pagefactory.interfaces.ITextField;

public class Element extends Container implements IBody, IButton, ICell, ICheckbox, IDiv, IElement, IFileField, IForm,
		IFrame, IInput, ILink, IOption, IRadio, IRow, ISelect, ISpan, ITable, ITextField {

	protected ElementLocator elementLocator;
	protected HashMap<String, Object> options;
	public String border;
	public static int TIMEOUT = 60;

	public Element(Container container, HashMap<String, Object> options) {
		this.container = container;
		this.methodInfo = (String) options.remove("methodInfo");
		this.options = options;
	}

	@Override
	protected Element locate() {
		if (options.get("element") != null && options.get("element") instanceof WebElement) {
			current = (WebElement) options.get("element");
			if (methodInfo == null) {
				methodInfo = container.methodInfo + ".(\"element=>" + current + "\")";
			}
		} else if (current == null) {
			try {
				elementLocator = new ElementLocator(container.wd(), options);
				// 定位最长等待60s
				current = (new WebDriverWait(container.driver, TIMEOUT)).until(new ExpectedCondition<WebElement>() {
					@Override
					public WebElement apply(WebDriver input) {
						return elementLocator.locate();
					}
				});
				methodInfo = container.methodInfo + "." + methodInfo;
			} catch (StaleElementReferenceException ex) {
				current = null;
				options.remove("element");
				locate();
			} catch (Exception e) {
				this.driver = container.driver;
				methodInfo = container.methodInfo + "." + methodInfo;
				// 保留错误信息，以供后续调用
				if (container.errorInfo == null) {
					errorInfo = methodInfo + "定位失败！" + e.getMessage();
				} else {
					errorInfo = container.errorInfo;
				}
			}
		}
		try {
			this.driver = container.driver;
			if (current != null && (current.getAttribute("tagName") != null)
					&& (current.getAttribute("tagName").toLowerCase().equals("iframe")
							|| current.getAttribute("tagName").toLowerCase().equals("frame"))) {
				isFrame = true;
				container.hasFrame = true;
				driver.switchTo().frame(current);
			}
		} catch (StaleElementReferenceException ex) {
			current = null;
			options.remove("element");
			locate();
		}

		return this;
	}

	/**
	 * 定位并返回当前对象
	 */
	@Override
	public Container wd() {
		locate();
		return this;
	}

	/**
	 * 当前元素是否存在
	 * 
	 * @return
	 */
	protected Boolean assertExists() {
		locate();
		return current != null;
	}

	/**
	 * 指定时间内，寻找控件，如果找到直接返回true；超出时间后，直接返回false
	 * 
	 * @param timeout
	 *            最长等待时间
	 * @return true|false
	 * @Example browser.element("id=>su").exists(50);//50秒内查找控件是否存在
	 */
	public Boolean exists(int timeout) {
		int temp = TIMEOUT;
		TIMEOUT = timeout;
		Boolean exists = assertExists();
		TIMEOUT = temp;
		return exists;
	}

	/**
	 * 判断当前控件是否存在
	 * 
	 * @return true|false
	 * @remark 默认最大等待时间为1秒
	 * @Example browser.element("id=>su").exists();
	 */
	public Boolean exists() {
		int temp = TIMEOUT;
		TIMEOUT = 1;
		Boolean exists = assertExists();
		TIMEOUT = temp;
		return exists;
	}

	/**
	 * 返回文本值
	 * 
	 * @return 文本值
	 * @Example <div>browser.element("id=>su").text()</div>
	 */
	public String text() {
		String text = null;
		methodInfo += ".text()";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			// highlight(true);
			text = current.getText();
			// highlight(false);
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		// LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS,
		// driver);
		return text;
	}

	/**
	 * 当前控件的tagName属性值
	 * 
	 * @return tagName属性值
	 * @Example <div>browser.element("id=>source").tagName();</div>
	 */
	public String tagName() {
		String tagName = null;
		methodInfo += ".tagName()";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			tagName = current.getTagName();
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		// LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS,
		// driver);
		return tagName;
	}

	/**
	 * 当前控件的class属性值
	 * 
	 * @return class属性值
	 * @Example <div>browser.element("id=>source").className();</div>
	 */
	public String className() {
		String className = null;
		methodInfo += ".className()";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			className = current.getAttribute("class");
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		// LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS,
		// driver);
		return className;
	}

	/**
	 * 返回id属性值
	 * 
	 * @return id属性值
	 * @Example <div>browser.element("id=>source").id();</div>
	 */
	public String id() {
		String id = null;
		methodInfo += ".id()";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			id = current.getAttribute("id");
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		// LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS,
		// driver);
		return id;
	}

	/**
	 * 返回outerHTML属性值
	 * 
	 * @return outerHTML属性值
	 * @Example <div>browser.element("id=>source").outerHTML();</div>
	 */
	public String outerHTML() {
		String outerHTML = null;
		methodInfo += ".outerHTML()";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			outerHTML = (String) this.executeJs("return arguments[0].outerHTML;", current);
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		// LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS,
		// driver);
		return outerHTML;
	}

	/**
	 * 获取指定样式属性对应的值
	 * 
	 * @param name
	 *            样式的属性
	 * @return 属性值 (控件对应的值)
	 * @Example <div>browser.div("class=>bk_list,index=>1").element(
	 *          "tagName=>dt,text=>Y").parent().getStyle("color");
	 *          //读取Y仓的父控件的颜色</div>
	 */
	public String getStyle(String name) {
		String style = null;
		methodInfo += ".getStyle(\"" + name + "\")";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			style = current.getCssValue(name);
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		// LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS,
		// driver);
		return style;
	}

	/**
	 * 获取控件的指定属性值
	 * 
	 * @param name
	 *            属性名称
	 * @return 属性值
	 * @Example <div>browser.div("id=>bk_list").getAttribute("class")</div>
	 */
	public String getAttribute(String name) {
		String attributeValue = null;
		methodInfo += ".getAttribute(\"" + name + "\")";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			// highlight(true);
			attributeValue = current.getAttribute(name);
			// highlight(false);
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		// LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS,
		// driver);
		return attributeValue;
	}

	/**
	 * 当前控件是否可用
	 * 
	 * @return true|false
	 * @Example <div>browser.div("id=>bk").enable()</div>
	 */
	public Boolean enable() {
		Boolean enable = null;
		methodInfo += ".enable()";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			enable = current.isEnabled();
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		// LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS,
		// driver);
		return enable;
	}

	/**
	 * 查看控件是否可用，可用则直接返回，或者在指定时间内，等待控件可用，若超时则返回false
	 * 
	 * @param timeout
	 * @return true|false
	 * @Example <div>browser.div("id=>bk").enable(50);//50秒内等待，直到可用为止或者超时退出</div>
	 */
	public Boolean enable(int timeout) {
		Boolean enable = false;
		methodInfo += ".enable(" + timeout + ")";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			enable = (new WebDriverWait(container.driver, timeout)).until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver input) {
					return current.isEnabled();
				}
			});
		} catch (TimeoutException e) {
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		// LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS,
		// driver);
		return enable;

	}

	/**
	 * 判断控件元素是否可见
	 * 
	 * @return true|false
	 * @Example <div>browser.div("id=>bk").visible();</div>
	 */
	public Boolean visible() {
		Boolean visible = null;
		methodInfo += ".visible()";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			visible = current.isDisplayed();
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		// LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS,
		// driver);
		return visible;
	}

	/**
	 * 查看控件是否可见，可见则直接返回，或者在指定时间内，等待控件可见，若超时则返回false
	 * 
	 * @param timeout
	 * @return true|false
	 * @Example <div>browser.div("id=>bk").invisible(50);//50秒内等待，直到可见为止或者超时退出</div>
	 */
	public Boolean visible(int timeout) {
		Boolean visible = false;
		methodInfo += ".visible(" + timeout + ")";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			visible = (new WebDriverWait(container.driver, timeout)).until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver input) {
					return current.isDisplayed();
				}
			});
		} catch (TimeoutException e) {
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		// LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS,
		// driver);
		return visible;

	}

	/**
	 * 查看控件是否不可见，不可见则直接返回，或者在指定时间内，等待控件不可见，若超时则返回false
	 * 
	 * @param timeout
	 * @return true|false
	 * @Example <div>browser.div("id=>bk").invisible(50);//50秒内等待，直到不可见为止或者超时退出</
	 *          div >
	 */
	public Boolean invisible(int timeout) {
		Boolean invisible = false;
		methodInfo += ".invisible(" + timeout + ")";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			invisible = (new WebDriverWait(container.driver, timeout)).until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver input) {
					return !current.isDisplayed();
				}
			});
		} catch (TimeoutException e) {
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		// LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS,
		// driver);
		return invisible;

	}

	/**
	 * 获取当前控件的指定父控件对象
	 * 
	 * @param options
	 *            父控件的属性与属性值信息，支持正则匹配
	 * @return 父控件对象
	 * @remark 支持多个属性与正则配置，层层遍历当前控件的直接与间接父级节点
	 * @Example browser.div("class=>booking").element("tagName=>dd,text=>/.*\\d+/").parent("class=>bk_list")
	 *          //获取有座位的航班信息</div>
	 */
	@SuppressWarnings("rawtypes")
	public Element parent(String options) {
		Element parent = null;
		methodInfo += ".parent(\"" + options + "\")";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			HashMap<String, Object> selectors = toHash(options);
			selectors.remove("methodInfo");
			WebElement webElement = (WebElement) executeJs("return arguments[0].parentNode;", current);
			Boolean find = false;
			String actual = null;
			while (!"html".equals(webElement.getTagName())) {
				Iterator itr = selectors.entrySet().iterator();
				while (itr.hasNext()) {
					Map.Entry er = (Map.Entry) itr.next();
					if (er.getValue() instanceof String) {
						// System.out.println(er.getValue().toString());
						// System.out.println(webElement.getAttribute(er.getKey().toString()));
						actual = webElement.getAttribute(er.getKey().toString());
						if (er.getKey().toString().equals("tagName")) {
							actual = actual.toLowerCase();
						}
						if (actual == null) {
							continue;
						}
						if (er.getValue().toString().equals(actual)) {
							find = true;
						} else {
							find = false;
						}
					} else if (er.getValue() instanceof Pattern) {
						Pattern value = (Pattern) er.getValue();
						actual = webElement.getAttribute(er.getKey().toString());
						if (er.getKey().toString().equals("tagName")) {
							actual = actual.toLowerCase();
						}
						if (actual == null) {
							continue;
						}
						Matcher matcher = value.matcher(actual);
						if (matcher.matches() || matcher.find()) {
							find = true;
						} else {
							find = false;
							break;
						}
					}
				}
				if (!find)
					webElement = (WebElement) executeJs("return arguments[0].parentNode;", webElement);
				else
					break;
			}
			if (!find) {
				throw new Exception("找不到父控件");
			} else {
				selectors.put("element", webElement);
				selectors.put("methodInfo", methodInfo);
				parent = new Element(container, selectors).locate();
			}

		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		// LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS,
		// driver);
		return parent;
	}

	/**
	 * 获取当前控件对象的父控件对象
	 * 
	 * @return 父控件对象
	 * @Example browser.div("class=>booking").element("tagName=>dd,text=>/.*\\d+/").parent();
	 */
	public Element parent() {
		Element parent = null;
		methodInfo += ".parent()";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			if (!"html".equals(current.getTagName())) {
				WebElement webElement = (WebElement) executeJs("return arguments[0].parentNode;", current);
				HashMap<String, Object> selectors = new HashMap<String, Object>();
				selectors.put("element", webElement);
				selectors.put("methodInfo", methodInfo);
				parent = new Element(container, selectors).locate();
			}
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		// LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS,
		// driver);
		return parent;
	}

	/**
	 * 获取当前控件对象的所有子控件对象
	 * 
	 * @return List<Element>，子控件对象集合
	 * @Example List<Element> list = browser.element("id=>source").children();
	 */
	public List<Element> children() {
		List<Element> elementList = new ArrayList<Element>();
		List<WebElement> webElementList = new ArrayList<WebElement>();
		methodInfo += ".children()";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			HashMap<String, Object> selectors = toHash("xpath=>./*");
			webElementList = new ElementLocator(this, selectors).findAllByMultiple(selectors);
			// webElementList = current.findElements(By.xpath("./*"));
			int i = 0;
			for (WebElement webElement : webElementList) {
				HashMap<String, Object> elementselectors = new HashMap<String, Object>();
				elementselectors.put("element", webElement);
				elementselectors.put("methodInfo", methodInfo + ".get(" + i + ")");
				elementList.add(new Element(container, elementselectors).locate());
				i++;
			}
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		// LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS,
		// driver);
		return elementList;
	}

	/**
	 * 获取当前控件对象的前一个控件对象
	 * 
	 * @return 同级控件对象
	 * @Example <div>browser.table("class=>dataintable").getCell(2,
	 *          2).previous()// 得到第二行第一列的单元格</div>
	 *          <div>browser.div("class=>booking").element("tagName=>dd,text=>/.*\\d+/").previous().text();//
	 *          得到有座位的舱位名称</div>
	 */
	public Element previous() {
		Element previous = null;
		methodInfo += ".previous()";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			// System.out.println(executeJs("return
			// arguments[0].previousSibling;",
			// current).getClass().getSimpleName());
			// System.out.println(((ArrayList)
			// executeJs("return arguments[0].previousSibling;",
			// current)).get(0));
			WebElement element = (WebElement) executeJs(
					"var previousSibling = arguments[0].previousSibling; while(previousSibling && previousSibling.nodeType != 1){previousSibling = previousSibling.previousSibling;}return previousSibling;",
					current);
			// WebElement element = (WebElement)
			// executeJs("return arguments[0].previousSibling;", current);
			HashMap<String, Object> selectors = new HashMap<String, Object>();
			selectors.put("element", element);
			selectors.put("methodInfo", methodInfo);
			previous = new Element(container, selectors).locate();
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		// LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS,
		// driver);
		return previous;
	}

	/**
	 * 获取当前控件同级的下一个控件对象
	 * 
	 * @return 同级控件对象的下一个控件对象
	 * @Example browser.element("tagName=>dt,text=>Y").next().radio("index=>1").set(true);//选择Y舱位
	 */
	public Element next() {
		Element next = null;
		methodInfo += ".next()";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			WebElement element = (WebElement) executeJs(
					"var nextSibling = arguments[0].nextSibling; while(nextSibling && nextSibling.nodeType != 1){nextSibling = nextSibling.nextSibling;}return nextSibling;",
					current);
			HashMap<String, Object> selectors = new HashMap<String, Object>();
			selectors.put("element", element);
			selectors.put("methodInfo", methodInfo);
			next = new Element(container, selectors).locate();
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		// LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS,
		// driver);
		return next;
	}

	/**
	 * 获取当前控件对象的上个文本节点值
	 * 
	 * @return
	 */
	public String previousTextNodeValue() {
		String text = "";
		methodInfo += ".previousTextNodeValue()";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			text = (String) executeJs(
					"var previousSibling = arguments[0].previousSibling; while(previousSibling && previousSibling.nodeType != 3){previousSibling = previousSibling.previousSibling;}return previousSibling.nodeValue;",
					current);
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		// LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS,
		// driver);
		return text;
	}

	/**
	 * 当前控件对象的下个文本节点值
	 * 
	 * @return
	 */
	public String nextTextNodeValue() {
		String text = "";
		methodInfo += ".nextTextNodeValue()";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			text = (String) executeJs(
					"var nextSibling = arguments[0].nextSibling; while(nextSibling && nextSibling.nodeType != 3){nextSibling = nextSibling.nextSibling;}return nextSibling.nodeValue;",
					current);
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		// LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS,
		// driver);
		return text;
	}

	/**
	 * 获取当前控件对象下的符合selectors的所有子控件对象
	 * 
	 * @param selectors
	 *            属性值匹配
	 * @return List<Element>，当前控件对象的符合属性值匹配的所有子控件对象集合
	 * @remark 暂时不支持正则
	 * @Example List<Element> airList =
	 *          browser.div("class=>booking").getElementList("class=>bk_list");获取所有航班的控件
	 */
	public List<Element> getElementList(String selectors) {
		HashMap<String, Object> hashedSelectors = toHash(selectors);
		return getElementList(hashedSelectors);
	}

	/**
	 * 获取当前控件对象下的符合selectors的所有子控件对象的文本值列表
	 * 
	 * @param selectors
	 *            属性值匹配
	 * @return List<String>，当前控件对象的符合属性值匹配的所有子控件对象的文本值集合
	 * 
	 */
	protected List<Element> getElementList(HashMap<String, Object> selectors) {
		List<Element> elementList = new ArrayList<Element>();
		List<WebElement> webElementList = new ArrayList<WebElement>();
		if (selectors.get("methodInfo") == null)
			methodInfo += "getElementTextList(" + selectors.toString() + ")";
		else {
			methodInfo = (String) selectors.remove("methodInfo");
		}
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			webElementList = new ElementLocator(this, selectors).findAllByMultiple(selectors);
			// String xpath = elementLocator.buildXpath(selectors);
			// webElementList = current.findElements(By.xpath(xpath));
			int i = 0;
			for (WebElement webElement : webElementList) {
				HashMap<String, Object> elementselectors = new HashMap<String, Object>();
				elementselectors.put("element", webElement);
				elementselectors.put("methodInfo", methodInfo + ".get(" + i + ")");
				elementList.add(new Element(container, elementselectors).locate());
				i++;
			}
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		// LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS,
		// driver);
		return elementList;
	}

	/**
	 * 获取当前控件对象下的符合selectors的所有子控件对象的文本值列表
	 * 
	 * @param selectors
	 *            属性值匹配
	 * @return List<String>，当前控件对象的符合属性值匹配的所有子控件对象的文本值集合
	 * @remark 暂时不支持正则
	 * @Example List<String> airnoList =
	 *          browser.div("class=>booking").getElementTextList("class=>airnoclass");获取所有航班号
	 */
	public List<String> getElementTextList(String selectors) {
		HashMap<String, Object> hashedSelectors = toHash(selectors);
		return getElementTextList(hashedSelectors);
	}

	/**
	 * 获取当前控件对象下的符合selectors的所有子控件对象的文本值列表
	 * 
	 * @param selectors
	 *            属性值匹配
	 * @return List<String>，当前控件对象的符合属性值匹配的所有子控件对象的文本值集合
	 */
	protected List<String> getElementTextList(HashMap<String, Object> selectors) {
		if (selectors.get("methodInfo") == null)
			methodInfo += "getElementTextList(" + selectors.toString() + ")";
		else {
			methodInfo = (String) selectors.remove("methodInfo");
		}
		List<String> elementTextList = new ArrayList<String>();

		List<WebElement> webElementList = new ArrayList<WebElement>();
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			webElementList = new ElementLocator(this, selectors).findAllByMultiple(selectors);
			// String xpath = elementLocator.buildXpath(selectors);
			// List<WebElement> elementList =
			// current.findElements(By.xpath(xpath));
			for (WebElement element : webElementList) {
				elementTextList.add(element.getText());
			}
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		// LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS,
		// driver);
		return elementTextList;
	}

	/**
	 * 
	 * 传入js命令来设置属性值
	 * 
	 * @param js
	 *            arguments[0].属性 = 值
	 */
	@SuppressWarnings("unused")
	private void setAttribute(String js) {
		methodInfo += ".setAttribute(" + js + ")";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			executeJs(js, current);
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	/**
	 * 一次性获取所有属性与属性值
	 * 
	 * @return 属性与属性值
	 * @remark 主要用于节省执行时间以及方便一次性获取所有属性值
	 * @Example browser.div("id=>mainAcceptTips").getAttributes().get("style");
	 */
	public Map<String, String> getAttributes() {
		Map<String, String> attributes = new HashMap<String, String>();
		methodInfo += ".getAttributes()";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			String attrs = (String) executeJs(
					"var str; var attrs=arguments[0].attributes ;for(var i=0;i<attrs.length;i++){ attr=attrs[i]; str+=','+attr.name+'=>'+attr.value; };return str",
					current);
			attrs = attrs.replaceFirst(",", "");
			for (String attr : attrs.split(",")) {
				String[] nameValue = attr.split("=>");
				attributes.put(nameValue[0], nameValue[1]);
			}
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
		return attributes;
	}

	/**
	 * 设置属性值
	 * 
	 * @param property
	 *            属性
	 * @param value
	 *            属性的值
	 * @Example browser.frame("id=>main").form("id=>releaseform").frame("index=>1").element("tagName=>body").setAttribute("innerText",
	 *          "ggggg");//在当前的多功能输入框中设置对应的文本属性值
	 */
	public void setAttribute(String property, String value) {
		methodInfo += ".setAttribute(\"" + property + "\",\"" + value + "\")";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			executeJs("arguments[0]." + property + " = arguments[1];", current, value);
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	/**
	 * 设置文本属性值
	 * 
	 * @param value
	 *            文本属性与属性值
	 * @Example browser.frame("id=>main").form("id=>releaseform").frame("index=>1").element("tagName=>body").setInnerText("fffff");//在当前的多功能输入框中输入对应的文本
	 */
	public void setInnerText(String value) {
		methodInfo += ".setInnerText(\"" + value + "\")";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			executeJs("arguments[0].textContent = arguments[1];", current, value);
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	/**
	 * 鼠标左键点击当前的element
	 * 
	 * @Example browser.element("id=>su").click();
	 */
	public void click() {
		methodInfo += ".click()";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			if (current.isDisplayed()) {
				highlight(true);
				current.click();
				highlight(false);
			} else {
				executeAsyncJs("arguments[0].click();", current);
			}
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);

	}

	/**
	 * js方式鼠标左键点击当前的element
	 * 
	 * @remark 对于click()方法无法实现的，可以使用JS的方式来实现
	 * @Example browser.element("id=>su").clickJs()
	 */
	public void clickJs() {
		methodInfo += ".clickJs()";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			highlight(true);
			executeJs("arguments[0].click();", current);
			highlight(false);
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	/**
	 * 模拟事件触发(模拟鼠标、键盘操作，如onmouseover、onkeyup、onkeydown等)
	 * 
	 * @see http://www.w3school.com.cn/tags/html_ref_eventattributes.asp
	 * @param name
	 * @Example browser.element("id=>source").fireEvent("onmouseover");
	 */
	public void fireEvent(String name) {
		methodInfo += ".fireEvent(\"" + name + "\")";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			highlight(true);
			String eventName = name.replaceFirst("on", "");
			StringBuffer script = new StringBuffer("var element = arguments[0];");
			script.append("var name = arguments[1];");
			script.append("canBubble = (typeof(canBubble) == 'undefined') ? true : false;");
			script.append("var evt = document.createEvent('HTMLEvents');");
			script.append("evt.shiftKey = false;" + "evt.metaKey = false;");
			script.append("evt.altKey = false;" + "evt.ctrlKey = false;");
			script.append("evt.initEvent(name, canBubble, true);");
			script.append("element.dispatchEvent(evt);");
			executeJs(script.toString(), current, eventName);
			highlight(false);
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	/**
	 * 拖拽当前控件到指定的控件中去
	 * 
	 * @param element
	 *            指定的控件，即目标控件
	 * @Example browser.element("browser=>source").drag(browser.element("id=>goal"));
	 */
	public void drag(Element element) {
		methodInfo += ".drag(element)";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			highlight(true);
			Actions builder = new Actions(driver);
			Action action = builder.dragAndDrop(current, element.current).build();
			action.perform();
			highlight(false);
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	/**
	 * 双击当前元素
	 * 
	 * @Example browser.element("id=>su").doubleclick();
	 */
	public void doubleClick() {
		methodInfo += ".doubleClick()";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			highlight(true);
			Actions action = new Actions(driver);

			action.doubleClick(current);
			action.perform();
			highlight(false);
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	/**
	 * 滚动页面使当前控件显示到最上面或者最下面
	 * 
	 * @param top
	 *            true时为最上面，false时为最下面
	 * @remark 针对网页懒加载的，需要先执行该操作，以便该元素被加载成功，才可以进行后续操作
	 * @Example browser.element("id=>su").scrollIntoView(true);//移动控件元素到页面的顶部;browser.element("id=>su").scrollIntoView(false);//移动控件元素到页面的底部
	 */
	public void scrollIntoView(Boolean top) {
		methodInfo += ".scrollIntoView(" + top.toString() + ")";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			highlight(true);
			executeJs("arguments[0].scrollIntoView(arguments[1]);", current, top);
			highlight(false);
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	/**
	 * 聚焦到当前元素
	 * 
	 * @Example browser.element("id=>source").focus();
	 */
	public void focus() {
		methodInfo += ".focus()";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			highlight(true);
			executeJs("return arguments[0].focus();", current);
			highlight(false);
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	/**
	 * 模拟鼠标点击
	 * 
	 * @Example browser.element("id=>source").mouseClick();
	 */
	public void mouseClick() {
		methodInfo += ".mouseClick()";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			highlight(true);
			Actions builder = new Actions(driver);
			Action action = builder.moveToElement(current).click().build();
			action.perform();
			highlight(false);
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	/**
	 * 获取当前控件是否只读
	 * 
	 * @return true|false|readonly
	 */
	public String readonly() {
		String readonly = null;
		methodInfo += ".readonly()";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			readonly = current.getAttribute("readOnly");
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		// LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS,
		// driver);
		return readonly;
	}

	/**
	 * 设置当前控件是否可读
	 * 
	 * @param readonly
	 *            Boolean值，true表示只读，false表示可读可写
	 * @Example browser.textfield("id=>begindate").readonly(false);
	 */
	public void readonly(Boolean readonly) {
		methodInfo += ".readonly(" + readonly.toString() + ")";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			executeJs("arguments[0].readOnly = arguments[1];", current, readonly);
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	/**
	 * 当前控件的文本最大长度
	 * 
	 * @return
	 */
	public int maxLength() {
		int maxLength = 0;
		methodInfo += ".maxLength()";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			maxLength = Integer.parseInt(current.getAttribute("maxLength"));
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		// LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS,
		// driver);
		return maxLength;
	}

	/**
	 * 设置当前控件的最大文本长度
	 * 
	 * @param length
	 */
	public void maxlength(int length) {
		methodInfo += ".maxlength(" + length + ")";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			executeJs("arguments[0].maxLength = arguments[1];", current, String.valueOf(length));
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	/**
	 * 模拟键盘操作
	 * 
	 * @see Keys
	 * @param value
	 *            Keys提供的键盘操作或者字符串；普通字符或者键盘的特有的按钮,可以多个参数
	 * @Example browser.element("id=>source").sendKeys(Keys.ENTER);
	 *          browser.textfield("id=>loginname").sendKeys(Keys.CONTROL, "a");
	 *          browser.textfield("id=>loginname").sendKeys(Keys.CONTROL, "c");
	 *          browser.textfield("id=>loginname").sendKeys(Keys.CONTROL, "v");
	 */
	public void sendKeys(CharSequence... value) {
		String actualValue = "";
		for (CharSequence key : value) {
			if (key instanceof Keys) {
				actualValue += ((Keys) key).name();
			} else {
				actualValue += key.toString();
			}
		}
		methodInfo += ".sendKey(\"" + actualValue + "\")";
		// }
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			highlight(true);
			current.sendKeys(value);
			highlight(false);
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	/**
	 * 文本框内容输入
	 * 
	 * @param value
	 *            输入的内容
	 * @remark 会清空内容后再输入
	 * @Example browser.element("id=>loginname").set("aaa");
	 */
	public void set(String value) {
		methodInfo += ".set(\"" + value + "\")";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			highlight(true);
			current.click();
			current.clear();
			current.sendKeys(value);
			highlight(false);
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	/**
	 * 文本框内容加密输入，内容不会明文存在任何地方
	 * 
	 * @param value
	 *            输入的内容
	 * @remark 会清空内容后再输入
	 * @Example browser.element("id=>loginname").encrypSet("aaa");
	 */
	public void encrypSet(String value) {
		methodInfo += ".encrypSet(\"" + "***" + "\")";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			highlight(true);
			current.click();
			current.clear();
			current.sendKeys(value);
			highlight(false);
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	protected void highlight(Boolean isHighlight) {
		// 暂时屏蔽掉，影响速度
		return;

		// try {
		// driver.switchTo().alert();
		// return;
		// } catch (NoAlertPresentException e) {
		//
		// }
		//
		// try {
		// if (isHighlight) {
		// border = current.getCssValue("border");
		// executeJs("arguments[0].style.border = \"2px solid yellow\"",
		// current);
		// } else {
		// executeJs("arguments[0].style.border = \"" + border + "\"", current);
		// }
		// } catch (Exception e) {
		//
		// }

	}
	// protected static final Log logger = LogFactory.getLog(Element.class);

	@Override
	public void append(String value) {
		methodInfo += ".set(\"" + value + "\")";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			current.sendKeys(value);
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	@Override
	public void clear() {
		methodInfo += ".clear()";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			highlight(true);
			current.clear();
			highlight(false);
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	@Override
	public List<Element> getRowList() {
		HashMap<String, Object> temp = new HashMap<String, Object>();
		temp.put("tagName", "tr");
		temp.put("methodInfo", methodInfo + ".getRowList()");
		return getElementList(temp);
	}

	@Override
	public List<List<String>> getRowTextList() {
		methodInfo += ".getRowTextList()";
		List<List<String>> rowTextList = new ArrayList<List<String>>();
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			List<WebElement> rowElementList = current.findElements(By.xpath(".//tr"));
			for (WebElement rowElement : rowElementList) {
				List<String> cellTextList = new ArrayList<String>();
				List<WebElement> cellElementList = rowElement.findElements(By.xpath(".//th|.//td"));
				for (WebElement cellElement : cellElementList) {
					cellTextList.add(cellElement.getText());
				}
				rowTextList.add(cellTextList);
			}
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
		return rowTextList;
	}

	@Override
	public Cell getCell(int row, int cell) {
		Cell cellElement = this.row("index=>" + Integer.valueOf(row)).cell("index=>" + Integer.valueOf(cell));
		return cellElement;
	}

	@Override
	public void setOptionByValue(String value) {
		methodInfo += ".setOptionByValue(\"" + value + "\")";
		Boolean exists = false;
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			List<WebElement> optionList = current.findElements(By.tagName("option"));
			for (WebElement op : optionList) {
				if (value.equalsIgnoreCase(op.getAttribute("value"))) {
					op.click();
					exists = true;
					break;
				}
			}
			if (!exists) {
				throw new Exception("找不到选项：" + value);
			}
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);

	}

	@Override
	public void setOptionByText(String text) {
		methodInfo += ".setOptionByText(\"" + text + "\")";
		Boolean exists = false;
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			List<WebElement> optionList = current.findElements(By.tagName("option"));
			Pattern pattern = null;
			if (text.startsWith("/") && text.endsWith("/")) {
				pattern = Pattern.compile(text.substring(1, text.length() - 1));
			}
			for (WebElement op : optionList) {
				// System.out.println(text);
				// System.out.println(op.getText());
				// System.out.println(text.equalsIgnoreCase(op.getText().trim()));
				String actualText = op.getText();
				if (actualText == null) {
					continue;
				} else if (pattern == null && text.equalsIgnoreCase(actualText.trim())) {
					op.click();
					exists = true;
					break;
				} else if (pattern != null) {
					Matcher matcher = pattern.matcher(actualText.trim());
					if (matcher.matches() || matcher.find()) {
						op.click();
						exists = true;
						break;
					}
				}
			}
			if (!exists) {
				throw new Exception("找不到选项：" + text);
			}
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	@Override
	public void setOptionByIndex(int index) {
		methodInfo += ".setOptionByIndex(" + index + ")";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			List<WebElement> optionList = current.findElements(By.tagName("option"));
			optionList.get(index - 1).click();
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	@Override
	public void setOptionByRand(int start, int end) {
		Random random = new Random(start);
		int index = start + random.nextInt(end + 1 - start);
		methodInfo += ".setOptionByRand(" + start + "," + end + ")";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			List<WebElement> optionList = current.findElements(By.tagName("option"));
			optionList.get(index - 1).click();
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);

	}

	@Override
	public void setOptionByRand() {
		int start = 1;
		methodInfo += ".setOptionByRand()";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			List<WebElement> optionList = current.findElements(By.tagName("option"));
			Random random = new Random();
			int index = start + random.nextInt(optionList.size() + 1 - start);
			methodInfo += "在" + optionList.size() + "选项中随机到第" + index + "个选项";
			optionList.get(index - 1).click();
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);

	}

	@Override
	public List<String> getSelectedTextList() {
		methodInfo += ".getSelectedTextList()";
		List<String> selectedTextList = new ArrayList<String>();
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			List<WebElement> optionList = current.findElements(By.tagName("option"));
			for (WebElement element : optionList) {
				if (element.isSelected()) {
					selectedTextList.add(element.getText().toString());
				}
			}
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
		return selectedTextList;
	}

	@Override
	public List<String> getSelectedValueList() {
		List<String> selectedValueList = new ArrayList<String>();
		methodInfo += ".getSelectedValueList()";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			List<WebElement> optionList = current.findElements(By.tagName("option"));
			for (WebElement element : optionList) {
				if (element.isSelected()) {
					selectedValueList.add(element.getAttribute("value"));
				}
			}
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
		return selectedValueList;
	}

	@Override
	public List<Element> getOptionList() {
		HashMap<String, Object> temp = new HashMap<String, Object>();
		temp.put("tagName", ".//option");
		temp.put("methodInfo", methodInfo + "getOptionList()");
		return getElementList(temp);
	}

	@Override
	public List<String> getOptionTextList() {
		List<String> optionTextList = new ArrayList<String>();
		HashMap<String, Object> temp = new HashMap<String, Object>();
		temp.put("tagName", ".//option");
		temp.put("methodInfo", methodInfo + "getOptionTextList()");
		optionTextList = getElementTextList(temp);
		return optionTextList;
	}

	@Override
	public List<Element> getCellList() {
		HashMap<String, Object> temp = new HashMap<String, Object>();
		temp.put("tagName", ".//th|.//td");
		temp.put("methodInfo", methodInfo + "getCellList()");
		return getElementList(temp);
	}

	@Override
	public Map<String, Integer> getCellTextIndexMap() {
		Map<String, Integer> cellTextIndexMap = new HashMap<String, Integer>();
		List<String> cellTextList = new ArrayList<String>();
		HashMap<String, Object> temp = new HashMap<String, Object>();
		temp.put("tagName", ".//th|.//td");
		temp.put("methodInfo", methodInfo + ".getCellTextIndexMap()");
		cellTextList = getElementTextList(temp);
		for (int i = 0; i < cellTextList.size(); i++) {
			String text = cellTextList.get(i);
			cellTextIndexMap.put(text, i + 1);
		}
		return cellTextIndexMap;
	}

	@Override
	public List<String> getCellTextList() {
		List<String> cellTextList = new ArrayList<String>();
		HashMap<String, Object> temp = new HashMap<String, Object>();
		temp.put("tagName", ".//th|.//td");
		temp.put("methodInfo", methodInfo + ".getCellTextList()");
		cellTextList = getElementTextList(temp);
		return cellTextList;
	}

	@Override
	public void submit() {
		methodInfo += ".submit()";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			highlight(true);
			current.submit();
			highlight(false);
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	@Override
	public void check() {
		methodInfo += ".check()";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			highlight(true);
			int retry = 3;
			while (!current.isSelected() && retry > 0) {
				current.click();
				retry--;
			}
			highlight(false);
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);

	}

	@Override
	public void uncheck() {
		methodInfo += ".uncheck()";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			highlight(true);
			int retry = 3;
			while (current.isSelected() && retry > 0) {
				current.click();
				retry--;
			}
			highlight(false);

		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	@Override
	public void set(Boolean check) {
		methodInfo += ".set(" + check + ")";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			highlight(true);
			int retry = 3;
			while (current.isSelected() != check && retry > 0) {
				current.click();
				retry--;
			}
			highlight(false);
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	@Override
	public int index() {
		// TODO Auto-generated method stub
		return 0;
	}

}
