package com.angrytest.elements;

import java.util.HashMap;

import com.angrytest.containers.Container;
import com.angrytest.enums.RunResult;
import com.angrytest.enums.StepType;
import com.angrytest.exceptions.AngryException;
import com.angrytest.logs.LogModule;

public class FileField extends Element {

	public FileField(Container container, HashMap<String, Object> options) {
		super(container, options);
	}

	/**
	 * 上传文件
	 * @param path 文件的路径
	 * @Example
	 * browser.fileField("id=>upload").set("文件的地址");//建议把文件存放在工程目录下
	 */
	public void set(String path) {
		methodInfo += ".set(\"" + path + "\")";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			current.sendKeys(path);
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}
}
