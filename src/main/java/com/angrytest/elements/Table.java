package com.angrytest.elements;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.angrytest.containers.Container;
import com.angrytest.enums.RunResult;
import com.angrytest.enums.StepType;
import com.angrytest.exceptions.AngryException;
import com.angrytest.logs.LogModule;

public class Table extends Element {
	public Table(Container container, HashMap<String, Object> options) {
		super(container, options);
	}

	/**
	 * 获取当前table的所有行的元素集合
	 * @return List<Element>,指定行的元素集合
	 * @Example 
	 * List<Element> bb = browser.table("class=>dataintable").getRowList();
	 */
	public List<Element> getRowList() {
		HashMap<String, Object> temp = new HashMap<String, Object>();
		temp.put("tagName", "tr");
		temp.put("methodInfo", methodInfo + ".getRowList()");
		return getElementList(temp);
	}

	/**
	 * 获取当前表格中的所有行的文本内容
	 * @return List<List<String>>，每一行是一个List,行与行形成总的List
	 * @Example List<List<String>> content= browser.table("class=>dataintable").getRowTextList();
	 */
	public List<List<String>> getRowTextList() {
		methodInfo += ".getRowTextList()";
		List<List<String>> rowTextList = new ArrayList<List<String>>();
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			List<WebElement> rowElementList = current.findElements(By.xpath(".//tr"));
			for (WebElement rowElement : rowElementList) {
				List<String> cellTextList = new ArrayList<String>();
				List<WebElement> cellElementList = rowElement.findElements(By.xpath(".//th|.//td"));
				for (WebElement cellElement : cellElementList) {
					cellTextList.add(cellElement.getText());
				}
				rowTextList.add(cellTextList);
			}
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
		return rowTextList;
	}

	/**
	 * 取得当前table的指定index的行的指定index列
	 * @param row 行号(从1开始)
	 * @param cell列号 (从1开始)
	 * @return Element
	 * @Example 
	 * browser.table("class=>dataintable").getCell(2, 2); //table的第二行第二列
	 */
	public Cell getCell(int row, int cell) {
		Cell cellElement = this.row("index=>" + Integer.valueOf(row)).cell("index=>" + Integer.valueOf(cell));
		return cellElement;
	}
}
