package com.angrytest.elements;

import java.util.HashMap;

import com.angrytest.containers.Container;
import com.angrytest.enums.RunResult;
import com.angrytest.enums.StepType;
import com.angrytest.exceptions.AngryException;
import com.angrytest.logs.LogModule;

public class Checkbox extends Element {
	public Checkbox(Container container, HashMap<String, Object> options) {
		super(container, options);
	}

	/**
	 * 选中
	 */
	public void check() {
		methodInfo += ".check()";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			highlight(true);
			int retry = 3;
			while (!current.isSelected() && retry > 0) {
				current.click();
				retry--;
			}
			highlight(false);
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	/**
	 * 取消选中
	 */
	public void uncheck() {
		methodInfo += ".uncheck()";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			highlight(true);
			int retry = 3;
			while (current.isSelected() && retry > 0) {
				current.click();
				retry--;
			}
			highlight(false);

		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	/**
	 * 设置复选框选中状态,参数为true表示选中,false表示不选中
	 * @param check boolean值
	 * @Example 
	 * browser.checkbox("id=>source").set(true);
	 */
	public void set(Boolean check) {
		methodInfo += ".set(" + check + ")";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			highlight(true);
			int retry = 3;
			while (current.isSelected() != check && retry > 0) {
				current.click();
				retry--;
			}
			highlight(false);
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}
}
