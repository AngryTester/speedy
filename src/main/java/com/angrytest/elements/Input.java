package com.angrytest.elements;

import java.util.HashMap;

import com.angrytest.containers.Container;
import com.angrytest.enums.RunResult;
import com.angrytest.enums.StepType;
import com.angrytest.exceptions.AngryException;
import com.angrytest.logs.LogModule;


public class Input extends Element {

	public Input(Container container, HashMap<String, Object> options) {
		super(container, options);
	}

	/**
	 * 文本框输入
	 * 
	 * @param value
	 */
	public void set(String value) {
		methodInfo += ".set(\"" + value + "\")";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			current.clear();
			current.sendKeys(value);
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}
}
