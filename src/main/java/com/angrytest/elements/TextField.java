package com.angrytest.elements;

import java.util.HashMap;

import com.angrytest.containers.Container;
import com.angrytest.enums.RunResult;
import com.angrytest.enums.StepType;
import com.angrytest.exceptions.AngryException;
import com.angrytest.logs.LogModule;

public class TextField extends Element {
	public TextField(Container container, HashMap<String, Object> options) {
		super(container, options);
	}

	/**
	 * 文本框内容输入
	 * 
	 * @param value 输入的内容
	 * @remark 会清空内容后再输入
	 * @Example browser.textfield("id=>loginname").set("aaa");
	 */
	public void set(String value) {
		methodInfo += ".set(\"" + value + "\")";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			highlight(true);
			current.click();
			current.clear();
			current.sendKeys(value);
			highlight(false);
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	/**
	 * 文本框内容追加输入，会在文本框的当前内容后面，追加输入
	 * 
	 * @param value 追加的内容
	 * @Example 
	 * browser.textfield("id=>loginname").append("aaa");
	 */
	public void append(String value) {
		methodInfo += ".set(\"" + value + "\")";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			current.sendKeys(value);
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	/**
	 * 清除文本框内容
	 */
	public void clear() {
		methodInfo += ".clear()";
		if (!assertExists()) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
			throw new AngryException(errorInfo);
		}
		try {
			highlight(true);
			current.clear();
			highlight(false);
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!" + e.getMessage());
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}
}
