package com.angrytest.logs;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

import com.angrytest.enums.RunResult;
import com.angrytest.enums.StepType;
import com.angrytest.reportng.HTMLReporter;
import com.angrytest.utils.DateUtil;
import com.angrytest.utils.ScreenShotUtil;

public class LogModule {

	public static List<LogStepInfo> onLogStep(LogStepInfo logStepInfo) {
		logStepInfo.setStepId(HTMLReporter.logStepInfoList.size() + 1);
		HTMLReporter.logStepInfoList.add(logStepInfo);
		return HTMLReporter.logStepInfoList;
	}

	/**
	 * 通用步骤失败日志
	 * 
	 * @param stepType
	 * @param stepDesc
	 * @param stepResult
	 * @param failReason
	 * @return
	 */
	public static List<LogStepInfo> logStepFail(StepType stepType, String stepDesc, RunResult stepResult,
			String failReason) {
		LogStepInfo logStepInfo = new LogStepInfo();
		logStepInfo.setStepType(stepType);
		logStepInfo.setStepDesc(stepDesc.replace("<", "&lt;").replace(">", "&gt;"));
		logStepInfo.setStepResult(stepResult);
		logStepInfo.setFailReason(failReason);
		String url = "";
		logStepInfo.setUrl(url);
		String stepTime = DateUtil.dateToStr(new Date(), "HH:mm:ss");
		logStepInfo.setStepTime(stepTime);
		String failType = "";
		logStepInfo.setFailType(failType);
		System.err.println(stepDesc + "\n" + failReason);
		return onLogStep(logStepInfo);
	}

	/**
	 * 通用步骤失败日志 日志中输入期望值与实际值的assertEqual断言
	 * 
	 * @param stepType
	 * @param stepDesc
	 * @param actual
	 * @param expect
	 * @param stepResult
	 * @param failReason
	 * @return
	 */
	public static List<LogStepInfo> logStepFail(StepType stepType, String stepDesc, String actual, String expect,
			RunResult stepResult, String failReason) {
		LogStepInfo logStepInfo = new LogStepInfo();
		logStepInfo.setStepType(stepType);
		// logStepInfo.setStepDesc(stepDesc.replace("<","&lt;").replace(">","&gt;"));
		logStepInfo.setStepDesc(stepDesc);
		logStepInfo.setStepResult(stepResult);
		logStepInfo.setFailReason(failReason);
		String url = "";
		logStepInfo.setUrl(url);
		String stepTime = DateUtil.dateToStr(new Date(), "HH:mm:ss");
		logStepInfo.setStepTime(stepTime);
		String failType = "";
		logStepInfo.setFailType(failType);
		// System.err.println(stepDesc + "\n" + failReason);
		return onLogStep(logStepInfo);
	}

	public static List<LogStepInfo> logStepFail(StepType stepType, String stepDesc, RunResult stepResult,
			String failReason, String hWnd) {
		return logStepFail(stepType, stepDesc, stepResult, failReason, hWnd, "");
	}

	/**
	 * WINDOWS_UI步骤失败日志，暂时没对hWnd做处理，直接全屏截图
	 * 
	 * @param stepType
	 * @param stepDesc
	 * @param stepResult
	 * @param failReason
	 * @param hWnd
	 * @param failType
	 * @return
	 */
	public static List<LogStepInfo> logStepFail(StepType stepType, String stepDesc, RunResult stepResult,
			String failReason, String hWnd, String failType) {
		LogStepInfo logStepInfo = new LogStepInfo();
		List<LogStepInfo> logStepInfoList = logStepFail(stepType, stepDesc, stepResult, failReason);
		logStepInfo = logStepInfoList.remove(logStepInfoList.size() - 1);
		String url = "";
		logStepInfo.setUrl(url);
		String picture = "";
		picture = ScreenShotUtil.screenShotByDesktop();
		logStepInfo.setPicture(picture);
		if (!picture.equals("")) {
			String[] pictureAddress = picture.split("screenshot");
			String pictureName = pictureAddress[pictureAddress.length - 1].substring(1);
			logStepInfo.setPictureName(pictureName);
			String pictureRelative = "screenshot/" + pictureName;
			logStepInfo.setPictureRelative(pictureRelative);
		}
		logStepInfo.setFailType(failType);
		return onLogStep(logStepInfo);
	}

	public static List<LogStepInfo> logStepFail(StepType stepType, String stepDesc, RunResult stepResult,
			String failReason, WebDriver driver) {
		if(stepType == StepType.ACTION){
			Reporter.log(stepDesc + "操作失败!"+failReason);
		}else{
			Reporter.log(stepDesc+failReason);
		}
		return logStepFail(stepType, stepDesc, stepResult, failReason, driver, "");
	}

	public static List<LogStepInfo> logStepFail(StepType stepType, String stepDesc, String actual, String expect,
			RunResult stepResult, String failReason, WebDriver driver) {
		return logStepFail(stepType, stepDesc, actual, expect, stepResult, failReason, driver, "");
	}

	/**
	 * WEB_UI步骤失败日志
	 * 
	 */
	public static List<LogStepInfo> logStepFail(StepType stepType, String stepDesc, RunResult stepResult,
			String failReason, WebDriver driver, String failType) {
		LogStepInfo logStepInfo = new LogStepInfo();
		List<LogStepInfo> logStepInfoList = logStepFail(stepType, stepDesc, stepResult, failReason);
		logStepInfo = logStepInfoList.remove(logStepInfoList.size() - 1);
		String url = "";
		String picture = "";
		// 采用grid的时候截屏无效，只需使用driver截图
		url = driver.getCurrentUrl();
		picture = ScreenShotUtil.screenShotByDriver(driver);
		logStepInfo.setUrl(url);
		logStepInfo.setPicture(picture);
		if (!picture.equals("")) {
			String[] pictureAddress = picture.split("screenshot");
			String pictureName = pictureAddress[pictureAddress.length - 1].substring(1);
			logStepInfo.setPictureName(pictureName);
			String pictureRelative = "screenshot/" + pictureName;
			logStepInfo.setPictureRelative(pictureRelative);
			HTMLReporter.PIC = pictureRelative;
		}
		logStepInfo.setFailType(failType);
		return onLogStep(logStepInfo);
	}

	/**
	 * WEB_UI失败日志 日志中输入期望值与实际值的assertEqual断言
	 * 
	 * @param stepType
	 * @param stepDesc
	 * @param actual
	 * @param expect
	 * @param stepResult
	 * @param failReason
	 * @param driver
	 * @param failType
	 * @return
	 */
	public static List<LogStepInfo> logStepFail(StepType stepType, String stepDesc, String actual, String expect,
			RunResult stepResult, String failReason, WebDriver driver, String failType) {
		LogStepInfo logStepInfo = new LogStepInfo();
		List<LogStepInfo> logStepInfoList = logStepFail(stepType, stepDesc, actual, expect, stepResult, failReason);
		logStepInfo = logStepInfoList.remove(logStepInfoList.size() - 1);
		String url = "";
		String picture = "";
		try {
			if (isAlert(driver)) {
				url = driver.switchTo().alert().getText();
				picture = ScreenShotUtil.screenShotByDesktop();

			} else {
				url = driver.getCurrentUrl();
				picture = ScreenShotUtil.screenShotByDriver(driver);
			}
		} catch (Exception e) {
			if (picture.equals("")) {
				try {
					picture = ScreenShotUtil.screenShotByDesktop();
				} catch (Exception ex) {
					ex.printStackTrace();
				}

			}
		}
		logStepInfo.setUrl(url);
		logStepInfo.setStepDesc(stepDesc);
		logStepInfo.setPicture(picture);
		if (!picture.equals("")) {
			String[] pictureAddress = picture.split("screenshot");
			String pictureName = pictureAddress[pictureAddress.length - 1].substring(1);
			logStepInfo.setPictureName(pictureName);
			String pictureRelative = "screenshot/" + pictureName;
			logStepInfo.setPictureRelative(pictureRelative);
		}
		logStepInfo.setFailType(failType);
		return onLogStep(logStepInfo);
	}

	/**
	 * 通用步骤成功日志
	 * 
	 * @param stepType
	 * @param stepDesc
	 * @param stepResult
	 * @return
	 */
	public static List<LogStepInfo> logStepPass(StepType stepType, String stepDesc, RunResult stepResult) {
		LogStepInfo logStepInfo = new LogStepInfo();
		logStepInfo.setStepType(stepType);
		logStepInfo.setStepDesc(stepDesc.replace("<", "&lt;").replace(">", "&gt;"));
		logStepInfo.setStepResult(stepResult);
		String url = "";
		logStepInfo.setUrl(url);
		String stepTime = DateUtil.dateToStr(new Date(), "HH:mm:ss");
		logStepInfo.setStepTime(stepTime);
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(stepDesc);
		return onLogStep(logStepInfo);
	}

	/**
	 * 通用步骤成功日志 日志中输入期望值与实际值
	 * 
	 * @param stepType
	 * @param stepDesc
	 * @param actual
	 * @param expect
	 * @param stepResult
	 * @return
	 */
	public static List<LogStepInfo> logStepPass(StepType stepType, String stepDesc, String actual, String expect,
			RunResult stepResult) {
		LogStepInfo logStepInfo = new LogStepInfo();
		logStepInfo.setStepType(stepType);
		logStepInfo.setStepDesc(stepDesc);
		logStepInfo.setStepResult(stepResult);
		String url = "";
		logStepInfo.setUrl(url);
		String stepTime = DateUtil.dateToStr(new Date(), "HH:mm:ss");
		logStepInfo.setStepTime(stepTime);
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(stepDesc);
		return onLogStep(logStepInfo);
	}

	/**
	 * WINDOWS_UI步骤成功日志
	 * 
	 * @param stepType
	 * @param stepDesc
	 * @param stepResult
	 * @param hWnd
	 * @return
	 */
	public static List<LogStepInfo> logStepPass(StepType stepType, String stepDesc, RunResult stepResult, String hWnd) {
		LogStepInfo logStepInfo = new LogStepInfo();
		List<LogStepInfo> logStepInfoList = logStepPass(stepType, stepDesc, stepResult);
		logStepInfo = logStepInfoList.remove(logStepInfoList.size() - 1);
		String url = "";
		logStepInfo.setUrl(url);
		return onLogStep(logStepInfo);
	}

	/**
	 * WEB_UI步骤成功日志
	 * 
	 * @param stepType
	 * @param stepDesc
	 * @param stepResult
	 * @param driver
	 * @return
	 */
	public static List<LogStepInfo> logStepPass(StepType stepType, String stepDesc, RunResult stepResult,
			WebDriver driver) {
		Reporter.log(stepDesc);
		LogStepInfo logStepInfo = new LogStepInfo();
		List<LogStepInfo> logStepInfoList = logStepPass(stepType, stepDesc, stepResult);
		logStepInfo = logStepInfoList.remove(logStepInfoList.size() - 1);
		String url = "";
		try {
			if (isAlert(driver)) {
				url = "alert";
			} else {
				url = driver.getCurrentUrl();
			}
		} catch (Exception e) {
		}
		logStepInfo.setUrl(url);
		return onLogStep(logStepInfo);
	}

	/**
	 * 判断弹出框是否存在
	 * 
	 * @param driver
	 * @return
	 */
	public static boolean isAlert(WebDriver driver) {
		try {
			driver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException e) {
			return false;
		}
	}

	/**
	 * @param str
	 * @param encoding
	 * @return 编码后的string
	 */
	public static String encode(String str, String encoding) {
		try {
			return URLEncoder.encode(str, encoding);
		} catch (UnsupportedEncodingException e) {
			return str;
		}
	}

}
