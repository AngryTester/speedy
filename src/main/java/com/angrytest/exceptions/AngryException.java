package com.angrytest.exceptions;

/**
 * 自定义异常
 * 
 * @ClassName: AngryException
 * @Description: 自定义异常
 * @author AngryTester
 * @date 2017年5月25日 下午3:51:50
 * 
 */
public class AngryException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AngryException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AngryException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public AngryException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public AngryException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
}
