package com.angrytest.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 无数据驱动时用这个注解标识用例信息
 * 
 * @ClassName: CaseInfo
 * @Description: 无数据驱动时用这个注解标识用例信息
 * @author AngryTester
 * @date 2017年6月19日 上午9:20:43
 * 
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD })
public @interface CaseInfo {

	/**
	 * @return 用例名
	 */
	public String caseName() default "";

	/**
	 * @return 用例描述
	 */
	public String caseDesc() default "";
}
