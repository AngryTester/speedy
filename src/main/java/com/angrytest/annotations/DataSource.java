package com.angrytest.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.angrytest.enums.DataSourceType;

/**
 * 数据驱动注解
 * 
 * @ClassName: DataSource
 * @Description: 注解
 * @author AngryTester
 * @date 2017年5月17日 上午11:44:42
 * 
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD })
public @interface DataSource {

	/**
	 * @return 数据驱动类型，默认为excel
	 */
	public DataSourceType type() default DataSourceType.EXCEL;

	/**
	 * @return 接口请求模板文件
	 */
	public String temp() default "";

	/**
	 * @return 测试数据文件
	 */
	public String file() default "";

	/**
	 * @return sheet名
	 */
	public String sheetName() default "";
}
