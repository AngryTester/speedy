//=============================================================================
// Copyright 2006-2013 Daniel W. Dyer
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//=============================================================================

package com.angrytest.reportng;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.velocity.VelocityContext;
import org.testng.IClass;
import org.testng.IInvokedMethod;
import org.testng.IResultMap;
import org.testng.IRetryAnalyzer;
import org.testng.ISuite;
import org.testng.ISuiteResult;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.ITestAnnotation;
import org.testng.asserts.IAssert;
import org.testng.xml.XmlSuite;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.angrytest.annotations.DataSource;
import com.angrytest.browsers.Browser;
import com.angrytest.dataproviders.Provider;
import com.angrytest.enums.DataSourceType;
import com.angrytest.enums.DriverMode;
import com.angrytest.logs.LogStepInfo;
import com.angrytest.utils.Config;
import com.angrytest.utils.DateUtil;

/**
 * Enhanced HTML reporter for TestNG that uses Velocity templates to generate
 * its output.
 *
 * @author Daniel Dyer
 */
public class HTMLReporter extends AbstractReporter {
    private static final String FRAMES_PROPERTY = "org.uncommons.reportng.frames";
    private static final String ONLY_FAILURES_PROPERTY = "org.uncommons.reportng.failures-only";

    private static final String TEMPLATES_PATH = "reportng/templates/html/";
    private static final String INDEX_FILE = "index.html";
    private static final String SUITES_FILE = "suites.html";
    private static final String OVERVIEW_FILE = "overview.html";
    private static final String GROUPS_FILE = "groups.html";
    private static final String RESULTS_FILE = "results.html";
    private static final String OUTPUT_FILE = "output.html";
    private static final String CUSTOM_STYLE_FILE = "custom.css";

    private static final String SUITE_KEY = "suite";
    private static final String SUITES_KEY = "suites";
    private static final String GROUPS_KEY = "groups";
    private static final String RESULT_KEY = "result";
    private static final String FAILED_CONFIG_KEY = "failedConfigurations";
    private static final String SKIPPED_CONFIG_KEY = "skippedConfigurations";
    private static final String FAILED_TESTS_KEY = "failedTests";
    private static final String SKIPPED_TESTS_KEY = "skippedTests";
    private static final String PASSED_TESTS_KEY = "passedTests";
    private static final String ONLY_FAILURES_KEY = "onlyReportFailures";

    public static final String FILE_SEPARATOR = System.getProperties().getProperty("file.separator");

    public static final String REPORT_DIRECTORY_TEMP = "reports" + FILE_SEPARATOR
            + DateUtil.dateToStr(new Date(), "yyyyMMdd-HHmmss");

    // 报告放根目录
    private static final String REPORT_DIRECTORY = ".." + FILE_SEPARATOR + ".." + FILE_SEPARATOR
            + REPORT_DIRECTORY_TEMP;

    private static final Comparator<ITestNGMethod> METHOD_COMPARATOR = new TestMethodComparator();
    private static final Comparator<ITestResult> RESULT_COMPARATOR = new TestResultComparator();
    private static final Comparator<IClass> CLASS_COMPARATOR = new TestClassComparator();

    private static final String PROJECT_NAME = "projectName";// 项目名
    private static final String RUNNER = "runner";// 运行人
    private static final String PASS_PERCENT = "passPercent";// 通过率
    private static final String TOTAL_CASE = "totalCase";// 总用例数
    private static final String PASSED_CASE = "passedCase";// 通过用例数
    private static final String FAILED_CASE = "failedCase";// 失败用例数
    private static final String START_TIME = "startTime";// 开始时间
    private static final String END_TIME = "endTime";// 结束时间
    private static final String DURATION = "duration";// 运行时间

    private static final String PICTURE_MAP = "picture_map";// 运行时间

    Date startDate;
    Date endDate;

    public static DriverMode DRIVER_MODE;

    public static List<LogStepInfo> logStepInfoList = new ArrayList<LogStepInfo>();

    public static String PIC = "";
    public static Map<ITestResult, String> PIC_MAP = new HashMap<ITestResult, String>();

    @SuppressWarnings("rawtypes")
    public static Map<Long, Map<AssertionError, IAssert>> m_errors_map = new HashMap<Long, Map<AssertionError, IAssert>>();

    private String startTime;
    private String endTime;
    private String duration;

    private static String buildId = "";
    private String machineIp = "";

    private static Map<ITestResult, String> sid_map = new HashMap<ITestResult, String>();
    private static Map<ITestResult, String> mid_map = new HashMap<ITestResult, String>();
    private static Map<ITestResult, String> caseId_map = new HashMap<ITestResult, String>();

    private static Map<String, String> suites_map = new HashMap<String, String>();// 用来装已经入库的suite
    private static Map<String, String> modules_map = new HashMap<String, String>();// 用来装已经入库的module

    DateFormat format = new SimpleDateFormat("yyyyMMddHHmmssSSS");

    ReportNGUtils util = new ReportNGUtils();

    public HTMLReporter() {
        super(TEMPLATES_PATH);
    }

    /**
     * Generates a set of HTML files that contain data about the outcome of the
     * specified test suites.
     *
     * @param suites              Data about the test runs.
     * @param outputDirectoryName The directory in which to create the report.
     */
    public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outputDirectoryName) {

        removeEmptyDirectories(new File(outputDirectoryName));

        boolean useFrames = System.getProperty(FRAMES_PROPERTY, "true").equals("true");
        boolean onlyFailures = System.getProperty(ONLY_FAILURES_PROPERTY, "false").equals("true");

        File outputDirectory = new File(outputDirectoryName, REPORT_DIRECTORY);
        outputDirectory.mkdirs();

        try {
            if (useFrames) {
                createFrameset(outputDirectory);
            }
            createOverview(suites, outputDirectory, !useFrames, onlyFailures);
            createSuiteList(suites, outputDirectory, onlyFailures);
            createGroups(suites, outputDirectory);
            createResults(suites, outputDirectory, onlyFailures);
            createLog(outputDirectory, onlyFailures);
            copyResources(outputDirectory);
        } catch (Exception ex) {
            throw new ReportNGException("Failed generating HTML report.", ex);
        }
    }

    /**
     * Create the index file that sets up the frameset.
     *
     * @param outputDirectory The target directory for the generated file(s).
     */
    private void createFrameset(File outputDirectory) throws Exception {
        VelocityContext context = createContext();
        generateFile(new File(outputDirectory, INDEX_FILE), INDEX_FILE + TEMPLATE_EXTENSION, context);
    }

    private void createOverview(List<ISuite> suites, File outputDirectory, boolean isIndex, boolean onlyFailures)
            throws Exception {
        VelocityContext context = createContext();
        String projectName = Config.getString("project");
        String runner = Config.getString("runner");
        int totalCase = 0;
        int passedCase = 0;
        int failedCase = 0;
        for (ISuite suite : suites) {
            Map<String, ISuiteResult> m = suite.getResults();
            for (Map.Entry<String, ISuiteResult> entry : m.entrySet()) {
                totalCase += entry.getValue().getTestContext().getFailedTests().size()
                        // +
                        // entry.getValue().getTestContext().getSkippedTests().size()
                        + entry.getValue().getTestContext().getPassedTests().size();
                passedCase += entry.getValue().getTestContext().getPassedTests().size();
                failedCase += entry.getValue().getTestContext().getFailedTests().size();
            }
        }

        duration = DateUtil.getDuration(startDate, endDate);
        String passPercent = util.formatPercentage(passedCase, totalCase);
        context.put(PROJECT_NAME, projectName);
        context.put(RUNNER, runner);
        context.put(PASS_PERCENT, passPercent);
        context.put(TOTAL_CASE, totalCase);
        context.put(PASSED_CASE, passedCase);
        context.put(FAILED_CASE, failedCase);
        context.put(START_TIME, startTime);
        context.put(END_TIME, endTime);
        context.put(DURATION, duration);
        context.put(SUITES_KEY, suites);
        context.put(ONLY_FAILURES_KEY, onlyFailures);
        generateFile(new File(outputDirectory, isIndex ? INDEX_FILE : OVERVIEW_FILE),
                OVERVIEW_FILE + TEMPLATE_EXTENSION, context);
    }

    /**
     * Create the navigation frame.
     *
     * @param outputDirectory The target directory for the generated file(s).
     */
    private void createSuiteList(List<ISuite> suites, File outputDirectory, boolean onlyFailures) throws Exception {
        VelocityContext context = createContext();
        context.put(SUITES_KEY, suites);
        context.put(ONLY_FAILURES_KEY, onlyFailures);
        generateFile(new File(outputDirectory, SUITES_FILE), SUITES_FILE + TEMPLATE_EXTENSION, context);
    }

    /**
     * Generate a results file for each test in each suite.
     *
     * @param outputDirectory The target directory for the generated file(s).
     */
    private void createResults(List<ISuite> suites, File outputDirectory, boolean onlyShowFailures) throws Exception {
        int index = 1;
        for (ISuite suite : suites) {
            int index2 = 1;
            for (ISuiteResult result : suite.getResults().values()) {
                boolean failuresExist = result.getTestContext().getFailedTests().size() > 0
                        || result.getTestContext().getFailedConfigurations().size() > 0;
                if (!onlyShowFailures || failuresExist) {
                    VelocityContext context = createContext();
                    context.put(RESULT_KEY, result);
                    context.put(FAILED_CONFIG_KEY, sortByTestClass(result.getTestContext().getFailedConfigurations()));
                    context.put(SKIPPED_CONFIG_KEY,
                            sortByTestClass(result.getTestContext().getSkippedConfigurations()));
                    context.put(FAILED_TESTS_KEY, sortByTestClass(result.getTestContext().getFailedTests()));
                    context.put(SKIPPED_TESTS_KEY, sortByTestClass(result.getTestContext().getSkippedTests()));
                    context.put(PASSED_TESTS_KEY, sortByTestClass(result.getTestContext().getPassedTests()));
                    context.put(PICTURE_MAP, PIC_MAP);
                    String fileName = String.format("suite%d_test%d_%s", index, index2, RESULTS_FILE);
                    generateFile(new File(outputDirectory, fileName), RESULTS_FILE + TEMPLATE_EXTENSION, context);
                }
                ++index2;
            }
            ++index;
        }
    }

    /**
     * Group test methods by class and sort alphabetically.
     */
    private SortedMap<IClass, List<ITestResult>> sortByTestClass(IResultMap results) {
        SortedMap<IClass, List<ITestResult>> sortedResults = new TreeMap<IClass, List<ITestResult>>(CLASS_COMPARATOR);
        for (ITestResult result : results.getAllResults()) {
            List<ITestResult> resultsForClass = sortedResults.get(result.getTestClass());
            if (resultsForClass == null) {
                resultsForClass = new ArrayList<ITestResult>();
                sortedResults.put(result.getTestClass(), resultsForClass);
            }
            int index = Collections.binarySearch(resultsForClass, result, RESULT_COMPARATOR);
            if (index < 0) {
                index = Math.abs(index + 1);
            }
            resultsForClass.add(index, result);
        }
        return sortedResults;
    }

    /**
     * Generate a groups list for each suite.
     *
     * @param outputDirectory The target directory for the generated file(s).
     */
    private void createGroups(List<ISuite> suites, File outputDirectory) throws Exception {
        int index = 1;
        for (ISuite suite : suites) {
            SortedMap<String, SortedSet<ITestNGMethod>> groups = sortGroups(suite.getMethodsByGroups());
            if (!groups.isEmpty()) {
                VelocityContext context = createContext();
                context.put(SUITE_KEY, suite);
                context.put(GROUPS_KEY, groups);
                String fileName = String.format("suite%d_%s", index, GROUPS_FILE);
                generateFile(new File(outputDirectory, fileName), GROUPS_FILE + TEMPLATE_EXTENSION, context);
            }
            ++index;
        }
    }

    /**
     * Generate a groups list for each suite.
     *
     * @param outputDirectory The target directory for the generated file(s).
     */
    private void createLog(File outputDirectory, boolean onlyFailures) throws Exception {
        if (!Reporter.getOutput().isEmpty()) {
            VelocityContext context = createContext();
            context.put(ONLY_FAILURES_KEY, onlyFailures);
            generateFile(new File(outputDirectory, OUTPUT_FILE), OUTPUT_FILE + TEMPLATE_EXTENSION, context);
        }
    }

    /**
     * Sorts groups alphabetically and also sorts methods within groups
     * alphabetically (class name first, then method name). Also eliminates
     * duplicate entries.
     */
    private SortedMap<String, SortedSet<ITestNGMethod>> sortGroups(Map<String, Collection<ITestNGMethod>> groups) {
        SortedMap<String, SortedSet<ITestNGMethod>> sortedGroups = new TreeMap<String, SortedSet<ITestNGMethod>>();
        for (Map.Entry<String, Collection<ITestNGMethod>> entry : groups.entrySet()) {
            SortedSet<ITestNGMethod> methods = new TreeSet<ITestNGMethod>(METHOD_COMPARATOR);
            methods.addAll(entry.getValue());
            sortedGroups.put(entry.getKey(), methods);
        }
        return sortedGroups;
    }

    /**
     * Reads the CSS and JavaScript files from the JAR file and writes them to
     * the output directory.
     *
     * @param outputDirectory Where to put the resources.
     * @throws IOException If the resources can't be read or written.
     */
    private void copyResources(File outputDirectory) throws IOException {
        copyClasspathResource(outputDirectory, "reportng.css", "reportng.css");
        copyClasspathResource(outputDirectory, "reportng.js", "reportng.js");
        copyClasspathResource(outputDirectory, "jquery-1.7.1.min.js", "jquery-1.7.1.min.js");
        copyClasspathResource(outputDirectory, "ichart.latest.min.js", "ichart.latest.min.js");
        // If there is a custom stylesheet, copy that.
        File customStylesheet = META.getStylesheetPath();

        if (customStylesheet != null) {
            if (customStylesheet.exists()) {
                copyFile(outputDirectory, customStylesheet, CUSTOM_STYLE_FILE);
            } else {
                // If not found, try to read the file as a resource on the
                // classpath
                // useful when reportng is called by a jarred up library
                InputStream stream = ClassLoader.getSystemClassLoader().getResourceAsStream(customStylesheet.getPath());
                if (stream != null) {
                    copyStream(outputDirectory, stream, CUSTOM_STYLE_FILE);
                }
            }
        }
    }

    @Override
    public void onStart(ISuite suite) {

        for (IInvokedMethod method : suite.getAllInvokedMethods()) {
            System.out.println(method.getTestMethod().getMethodName());
        }

        Reporter.log("套件" + suite.getName() + "执行开始");
        System.out.println("套件" + suite.getName() + "执行开始");
        int flag = 0;
        if (flag == 0) {
            startDate = new Date();
            startTime = DateUtil.dateToStr(DateUtil.getToday(), "yyyy-MM-dd HH:mm:ss");
            flag = 1;
        }
    }

    @Override
    public void onFinish(ISuite suite) {
        endDate = new Date();
        endTime = DateUtil.dateToStr(DateUtil.getToday(), "yyyy-MM-dd HH:mm:ss");
        Browser.closeAll();// 套件执行结束退出所有driver
        Reporter.log("套件" + suite.getName() + "执行结束");
        System.out.println("套件" + suite.getName() + "执行结束");
    }

    @SuppressWarnings("rawtypes")
    @Override
    public void transform(ITestAnnotation annotation, Class testClass, Constructor testConstructor, Method testMethod) {
        if (testMethod.getAnnotation(DataSource.class) != null) {
            annotation.setDataProviderClass(Provider.class);
            DataSourceType type = ((DataSource) testMethod.getAnnotation(DataSource.class)).type();
            switch (type) {
                case EXCEL:
                    if (testMethod.getParameterTypes().length == 2) {
                        annotation.setDataProvider("excel");
                    } else {
                        annotation.setDataProvider("excel2");
                    }
                    break;
                case DB:
                    if (testMethod.getParameterTypes().length == 2) {
                        annotation.setDataProvider("db");
                    } else {
                        annotation.setDataProvider("db2");
                    }
                    break;
                default:
            }
        }
        IRetryAnalyzer retry = annotation.getRetryAnalyzer();
        if (retry == null) {
            annotation.setRetryAnalyzer(HTMLReporter.class);
        }
    }

    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
        Reporter.log("方法" + method.getTestMethod().getMethodName() + "执行结束");
        System.out.println("方法" + method.getTestMethod().getMethodName() + "执行结束");
        PIC_MAP.put(testResult, PIC);
        // 测试方法调用完关闭浏览器相关进程
        Browser.close(Thread.currentThread().getId());
        // 测试方法调用完清除当前线程断言信息
        if (!HTMLReporter.m_errors_map.isEmpty()) {
            if (HTMLReporter.m_errors_map.containsKey(Thread.currentThread().getId())) {
                HTMLReporter.m_errors_map.remove(Thread.currentThread().getId());
            }
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
        // 按照线程id将重试次数初始化
        if (!retryMap.containsKey(Thread.currentThread().getId())) {
            retryMap.put(Thread.currentThread().getId(), 0);
        }
        Reporter.log("方法" + method.getTestMethod().getMethodName() + "执行开始");
        System.out.println("方法" + method.getTestMethod().getMethodName() + "执行开始");
        PIC = "";
    }

    // 以下为失败重试功能实现

    private static Map<Long, Integer> retryMap = new HashMap<Long, Integer>();
    private static int maxRetryCount = 0;

    static {
        if (Config.containsIntKey("retry")) {
            maxRetryCount = Config.getInt("retry");
        } else {
            maxRetryCount = 0;
        }
    }

    private boolean isRetryAvailable(ITestResult result) {
        return retryMap.get(Thread.currentThread().getId()) < maxRetryCount;
    }

    public boolean retry(ITestResult result) {
        if (isRetryAvailable(result)) {
            int count = retryMap.remove(Thread.currentThread().getId());
            count++;
            System.out.println(result.getName() + "运行失败，下面进入" + "第" + count + "次" + "重运行");
            Reporter.log(result.getName() + "运行失败，下面进入" + "第" + count + "次" + "重运行");
            retryMap.put(Thread.currentThread().getId(), count);
            return true;
        }
        // retryCount = 0;
        return false;
    }
}
