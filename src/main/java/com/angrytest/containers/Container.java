package com.angrytest.containers;

import java.util.HashMap;
import java.util.regex.Pattern;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.angrytest.elements.Body;
import com.angrytest.elements.Button;
import com.angrytest.elements.Cell;
import com.angrytest.elements.Checkbox;
import com.angrytest.elements.Div;
import com.angrytest.elements.Element;
import com.angrytest.elements.FileField;
import com.angrytest.elements.Form;
import com.angrytest.elements.Frame;
import com.angrytest.elements.Input;
import com.angrytest.elements.Link;
import com.angrytest.elements.Option;
import com.angrytest.elements.Radio;
import com.angrytest.elements.Row;
import com.angrytest.elements.Select;
import com.angrytest.elements.Span;
import com.angrytest.elements.Table;
import com.angrytest.elements.TextField;
import com.angrytest.enums.RunResult;
import com.angrytest.enums.StepType;
import com.angrytest.logs.LogModule;

/**
 * 网页控件集合
 * 
 * @author
 * 
 */
public abstract class Container {
	public WebDriver driver;
	public Container container;
	public String methodInfo;
	public String errorInfo;
	public String currentTime;
	public boolean isFrame = false;
	public boolean hasFrame = false;

	public WebDriver getDriver() {
		return driver;
	}

	public WebElement current;

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	protected abstract Container locate();

	public abstract Container wd();

	/**
	 * 执行JS
	 * @param script
	 * @param args
	 * @return
	 */
	public Object executeJs(String script, Object... args) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		return js.executeScript(script, args);
	}

	/**
	 * 异步执行JS
	 * @param script
	 * @param args
	 * @return
	 */
	public Object executeAsyncJs(String script, Object... args) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		return js.executeAsyncScript(script, args);
	}

	/**
	 * 定位Button
	 * @param options String类型，属性与属性值
	 * @return Button
	 */
	public Button button(String options) {
		HashMap<String, Object> selectors = toHash(options);
		selectors.put("tagName", "button|input[@type='button'or @type='submit' or @type='image' or @type='reset']");
		Button button = new Button(this, selectors);
		return button;
	}

	/**
	 * 定位Element
	 * @param options String类型，属性与属性值
	 * @return Element
	 */
	public Element element(String options) { 
		Element element = new Element(this, toHash(options));
		return element;
	}

	/**
	 * 定位Div
	 * @param options String类型，属性与属性值
	 * @return Div
	 */
	public Div div(String options) {
		HashMap<String, Object> selectors = toHash(options);
		selectors.put("tagName", "div");
		Div element = new Div(this, selectors);
		return element;
	}

	/**
	 * 定位Span
	 * @param options String类型，属性与属性值
	 * @return Span
	 */
	public Span span(String options) {
		HashMap<String, Object> selectors = toHash(options);
		selectors.put("tagName", "span");
		Span element = new Span(this, selectors);
		return element;
	}

	/**
	 * 定位文本框
	 * @param options String类型，属性与属性值
	 * @return TextField
	 */
	public TextField textfield(String options) {

		HashMap<String, Object> selectors = toHash(options);
		selectors.put("tagName", "input|textarea");
		TextField element = new TextField(this, selectors);
		return element;
	}

	/**
	 * 定位表格
	 * @param options String类型，属性与属性值
	 * @return Table
	 * @Example browser.table("class=>dataintable");
	 */
	public Table table(String options) {
		HashMap<String, Object> selectors = toHash(options);
		selectors.put("tagName", "table");
		Table element = new Table(this, selectors);
		return element;
	}

	/**
	 * 定位列
	 * @param options String类型，属性与属性值
	 * @return Cell
	 */
	public Cell cell(String options) {
		HashMap<String, Object> selectors = toHash(options);
		selectors.put("tagName", "th|td");
		Cell element = new Cell(this, selectors);

		return element;
	}

	/**
	 * 定位行
	 * @param options String类型，属性与属性值
	 * @return Row
	 */
	public Row row(String options) {
		HashMap<String, Object> selectors = toHash(options);
		selectors.put("tagName", "tr");
		Row element = new Row(this, selectors);
		return element;
	}

	public Input input(String options) {
		HashMap<String, Object> selectors = toHash(options);
		selectors.put("tagName", "input");
		Input element = new Input(this, selectors);
		return element;
	}

	/**
	 * 定位多选框
	 * @param options String类型，属性与属性值
	 * @return Checkbox
	 */
	public Checkbox checkbox(String options) {
		HashMap<String, Object> selectors = toHash(options);
		selectors.put("tagName", "input");
		Checkbox element = new Checkbox(this, selectors);
		return element;
	}

	/**
	 * 定位单选框
	 * @param options String类型，属性与属性值
	 * @return Radio
	 */
	public Radio radio(String options) {
		HashMap<String, Object> selectors = toHash(options);
		selectors.put("tagName", "input");
		Radio element = new Radio(this, selectors);
		return element;
	}

	/**
	 * 定位下拉框
	 * @param options String类型，属性与属性值
	 * @return Select
	 */
	public Select select(String options) {
		HashMap<String, Object> selectors = toHash(options);
		selectors.put("tagName", "select");
		Select element = new Select(this, selectors);
		return element;
	}

	/**
	 * 定位下拉框选项
	 * @param options String类型，属性与属性值
	 * @return Option
	 */
	public Option option(String options) {
		HashMap<String, Object> selectors = toHash(options);
		selectors.put("tagName", "option");
		Option element = new Option(this, selectors);
		return element;
	}

	/**
	 * 定位表单
	 * @param options String类型，属性与属性值
	 * @return Form
	 * @Example
	 * browser.div("id=>loginchina").frame("index=>1").form("id=>J_StaticForm").textfield("id=>TPL_username_1").set("sssss");
	 */
	public Form form(String options) {
		HashMap<String, Object> selectors = toHash(options);
		selectors.put("tagName", "form");
		Form element = new Form(this, selectors);
		return element;
	}

	/**
	 * 定位链接
	 * @param options String类型，属性与属性值
	 * @return Link
	 */
	public Link link(String options) {
		HashMap<String, Object> selectors = toHash(options);
		selectors.put("tagName", "a");
		Link element = new Link(this, selectors);
		return element;
	}

	public Body body(String options) {
		HashMap<String, Object> selectors = toHash(options);
		selectors.put("tagName", "body");
		Body element = new Body(this, selectors);
		return element;
	}

	/**
	 * 定位文件
	 * @param options String类型，属性与属性值
	 * @return FileField
	 */
	public FileField fileField(String options) {
		HashMap<String, Object> selectors = toHash(options);
		selectors.put("type", "file");
		selectors.put("tagName", "input");
		FileField element = new FileField(this, selectors);
		return element;
	}

	/**
	 * 当要操作的对象在frame或者iframe下，需要先定位到frame然后再做控件的操作
	 * @param options String类型，index或者各种frame拥有的属性值
	 * @return Frame
	 * @Example 
	 * browser.frame("id=>leftFrame").div("id=>left").link("text=>内容管理").click();
	 */
	public Frame frame(String options) {

		HashMap<String, Object> selectors = toHash(options);
		selectors.put("tagName", "iframe|frame");
		Frame element = new Frame(this, selectors);
		return element;
	}

	public HashMap<String, Object> toHash(String options) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		String methodInfo = Thread.currentThread().getStackTrace()[2].getMethodName() + "(\"" + options + "\")";
		map.put("methodInfo", methodInfo);
		if (options.contains("xpath=>")) {
			map.put("xpath", options.substring(options.indexOf("=>") + 2));
			return map;
		}
		String[] optionInfos = options.split(",");
		for (String optionInfo : optionInfos) {
			if (optionInfo.indexOf("=>") < 0) {
				errorInfo = options + "格式错误,请参照（属性=>属性值）,示例：ie.button(\"id=>kw\").click();";
				LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
				throw new RuntimeException(errorInfo);
				// 抛出格式错误
			}
			String[] option = optionInfo.split("=>");
			if (option.length == 2) {
				if (option[1].startsWith("/") && option[1].endsWith("/")) {
					Pattern pattern = Pattern.compile(option[1].substring(1, option[1].length() - 1), Pattern.DOTALL);
					map.put(option[0], pattern);
				} else {
					map.put(option[0], option[1]);
				}
			} else {// 抛出格式错误}
				errorInfo = options + "格式错误,请参照（属性=>属性值）,多个属性键值匹配用逗号分隔,示例：ie.button(\"name=>aa,type=>submit,index=>1\").click();";
				LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, errorInfo, driver);
				throw new RuntimeException(errorInfo);
			}
		}
		return map;
	}

}
