package com.angrytest.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.csvreader.CsvReader;

/**
 * Excel处理工具类
 * 
 * @ClassName: ExcelUtil
 * @Description: Excel处理工具类
 * @author AngryTester
 * @date 2017年5月25日 下午3:53:03
 * 
 */
public class ExcelUtil {

	/** 总行数 */
	private int totalRows = 0;

	/** 总列数 */
	private int totalCells = 0;

	/** 错误信息 */
	private String errorInfo;

	/**
	 * 判断是否为xls文件
	 * 
	 * @param filePath
	 * @return
	 */
	private boolean isExcel2003(String filePath) {
		return filePath.matches("^.+\\.(?i)(xls)$");
	}

	/**
	 * 判断是否为xlsx文件
	 * 
	 * @param filePath
	 * @return
	 */
	private boolean isExcel2007(String filePath) {
		return filePath.matches("^.+\\.(?i)(xlsx)$");
	}

	/**
	 * 获取总列数
	 * 
	 * @return
	 */
	private int getTotalCells() {
		return totalCells;
	}

	/**
	 * 验证Excel格式是否符合要求
	 * 
	 * @param filePath
	 * @return
	 */
	public boolean validateExcel(String filePath) {
		/** 检查文件名是否为空或者是否是Excel格式的文件 */
		if (filePath == null || !(isExcel2003(filePath) || isExcel2007(filePath))) {
			errorInfo = "文件名不是excel格式";
			return false;
		}
		/** 检查文件是否存在 */
		File file = new File(filePath);
		if (file == null || !file.exists()) {
			errorInfo = filePath + "文件不存在";
			return false;
		}
		return true;
	}

	/**
	 * 根据文件路径和sheet名解析
	 * 
	 * @param filePath
	 * @param sheetName
	 * @return
	 */
	public List<List<String>> read(String filePath, String sheetName) {
		List<List<String>> dataLst = new ArrayList<List<String>>();
		InputStream is = null;
		if (!validateExcel(filePath)) {
			System.out.println(errorInfo);
			return null;
		}
		boolean isExcel2003 = true;
		if (isExcel2007(filePath)) {
			isExcel2003 = false;
		}
		try {
			File file = new File(filePath);
			is = new FileInputStream(file);
			/** 根据版本选择创建Workbook的方式 */
			Workbook wb = null;
			if (isExcel2003) {
				wb = new HSSFWorkbook(is);
			} else {
				wb = new XSSFWorkbook(is);
			}
			dataLst = read(wb, sheetName);
			is.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					is = null;
					e.printStackTrace();
				}
			}
		}
		/** 返回最后读取的结果 */
		return dataLst;

	}

	/**
	 * 根据workbook对象和sheet编号解析
	 * 
	 * @param wb
	 * @param sheetIndex
	 * @return
	 */
	@SuppressWarnings("deprecation")
	private List<List<String>> read(Workbook wb, int sheetIndex) {
		List<List<String>> dataLst = new ArrayList<List<String>>();
		/** 得到指定的sheet */
		Sheet sheet = wb.getSheetAt(sheetIndex);
		/** 得到Excel的行数 */
		this.totalRows = sheet.getPhysicalNumberOfRows();
		/** 得到Excel的列数 */
		if (totalRows >= 1 && sheet.getRow(0) != null) {
			totalCells = sheet.getRow(0).getPhysicalNumberOfCells();
		}
		/** 循环Excel的行 */
		for (int r = 0; r < this.totalRows; r++) {
			Row row = sheet.getRow(r);
			if (row == null) {
				continue;
			}
			List<String> rowLst = new ArrayList<String>();
			/** 循环Excel的列 */
			for (int c = 0; c < this.getTotalCells(); c++) {
				Cell cell = row.getCell(c);
				String cellValue = "";
				if (null != cell) {
					// 以下是判断数据的类型
					switch (cell.getCellType()) {
					case HSSFCell.CELL_TYPE_NUMERIC: // 数字
						// cellValue = cell.getNumericCellValue() + "";
						if (HSSFDateUtil.isCellDateFormatted(cell)) {
							// 如果是Date类型则，取得该Cell的Date值
							Date date = cell.getDateCellValue();
							// 把Date转换成本地格式的字符串
							cellValue = DateUtil.dateToStr(date, "yyyy-MM-dd HH:mm:ss").toString();
							System.out.println(cellValue);
						}
						// 如果是纯数字
						else {
							// 取得当前Cell的数值
							Integer num = new Integer((int) cell.getNumericCellValue());
							cellValue = String.valueOf(num);
						}
						break;
					case HSSFCell.CELL_TYPE_STRING: // 字符串
						cellValue = cell.getStringCellValue().trim();
						break;
					case HSSFCell.CELL_TYPE_BOOLEAN: // Boolean
						cellValue = cell.getBooleanCellValue() + "";
						break;
					case HSSFCell.CELL_TYPE_FORMULA: // 公式
						cellValue = cell.getCellFormula() + "";
						break;
					case HSSFCell.CELL_TYPE_BLANK: // 空值
						cellValue = "";
						break;
					case HSSFCell.CELL_TYPE_ERROR: // 故障
						cellValue = "非法字符";
						break;
					default:
						cellValue = "未知类型";
						break;
					}
				}
				rowLst.add(cellValue);
			}
			dataLst.add(rowLst);
		}
		return dataLst;
	}

	private List<List<String>> read(Workbook wb, String sheetName) {
		int sheetIndex = 0;
		try {
			sheetIndex = wb.getSheetIndex(sheetName);
		} catch (Exception e) {
			// 抛异常取第一个sheet
		}
		// 默认取第一个
		if (sheetIndex < 0) {
			sheetIndex = 0;
		}
		return read(wb, sheetIndex);
	}

	/**
	 * 将excel解析后的集合封装成Map形式
	 * 
	 * @param list
	 * @return
	 */
	public static List<Map<String, String>> reflectMapList(List<List<String>> list) {
		List<Map<String, String>> mlist = new ArrayList<Map<String, String>>();
		Map<String, String> map = new HashMap<String, String>();
		if (list != null) {
			for (int i = 1; i < list.size(); i++) {
				map = new HashMap<String, String>();
				List<String> cellList = list.get(i);
				for (int j = 0; j < cellList.size(); j++) {
					map.put(list.get(0).get(j), cellList.get(j));
				}
				mlist.add(map);
			}
		}

		return mlist;
	}

	public List<Map<String, String>> excelDatas(String filePath, String sheetName) {
		List<List<String>> lists = read(filePath, sheetName);
		// 对集合进行重新组装 Map<字段,值>
		List<Map<String, String>> datas = ExcelUtil.reflectMapList(lists);
		return datas;
	}

	/**
	 * 读取CSV的方法
	 * 
	 * @param file
	 * @return
	 */
	public List<String[]> importCsv(String file) {
		List<String[]> list = new ArrayList<String[]>();
		CsvReader reader = null;
		try {
			// 初始化CsvReader并指定列分隔符和字符编码
			reader = new CsvReader(file, ',', Charset.forName("GBK"));
			while (reader.readRecord()) {
				// 读取每行数据以数组形式返回
				String[] str = reader.getValues();
				if (str != null && str.length > 0) {
					// if (str[0] != null && !"".equals(str[0].trim())) {
					// list.add(str);
					// }
					if (str[0] != null) {
						list.add(str);
					}
				}
			}
		} catch (FileNotFoundException e) {
			// log.error("Error reading csv file.", e);
		} catch (IOException e) {
			// log.error("", e);
		} finally {
			if (reader != null)
				// 关闭CsvReader
				reader.close();
		}
		return list;
	}
}
