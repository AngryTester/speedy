package com.angrytest.utils;

import com.typesafe.config.ConfigException;
import com.typesafe.config.ConfigFactory;

/**
 * 获取配置文件值
 * 
 * @ClassName: Config
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author AngryTester
 * @date 2017年5月25日 下午3:53:48
 * 
 */
public class Config {

	/**
	 * 获取String类型键值
	 * @param key
	 * @return
	 */
	public static String getString(String key) {
		return ConfigFactory.load("test").getString(key);
	}

	/**
	 * 获取int类型键值
	 * @param key
	 * @return
	 */
	public static int getInt(String key) {
		return ConfigFactory.load("test").getInt(key);
	}

	/**
	 * 判断String类型键是否存在
	 * @param key
	 * @return
	 */
	public static boolean containsStringKey(String key) {
		try {
			Config.getString(key);
		} catch (ConfigException e) {
			return false;
		}
		return true;
	}

	/**
	 * 判断int类型键是否存在
	 * @param key
	 * @return
	 */
	public static boolean containsIntKey(String key) {
		try {
			Config.getInt(key);
		} catch (ConfigException e) {
			return false;
		}
		return true;
	}

}
