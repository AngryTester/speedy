package com.angrytest.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.DatatypeConverter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.angrytest.exceptions.AngryException;


/**
 * 文件处理工具类
 * 
 * @author AngryTester
 *
 */
public class FileUtil {
	
	protected static final Log logger = LogFactory.getLog(FileUtil.class);
	private static final String TEST_PROPERTIES = "/test.properties";

	/**
	 * 读取文件，返回字符串
	 * 
	 * @param fileName
	 * @return
	 */
	public static String readFile(String fileName) {
		String returnStr = "";
		File file = new File(fileName);
		Reader reader = null;
		try {
			// 一次读一个字符
			reader = new InputStreamReader(new FileInputStream(file), "UTF-8");
			int tempchar;
			while ((tempchar = reader.read()) != -1) {
				if (((char) tempchar) != '\r') {
					returnStr += (char) tempchar;
				}
			}
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnStr;
	}

	/**
	 * 根据templateFile和数据输入初始化请求内容，模板中的变量名必须与Map中的键一致
	 * 
	 * @param templateFile
	 * @param params
	 * @return
	 */
	public static String initRequestBody(String templateFile, Map<String, String> params) {
		String req = readFile(templateFile);
		// 替换请求相关字段
		Pattern p1 = Pattern.compile("\\$\\{\\#\\#.*?\\}");
		Matcher mat1 = p1.matcher(req);
		List<String> elements1 = new ArrayList<String>();

		while (mat1.find()) {
			String element = mat1.group();
			elements1.add(element);
		}

		try{
			for (String element : elements1) {
				int i = element.indexOf("${##");
				int j = element.indexOf("}");
				String sub = element.substring(i + 4, j);
				String param = "";
				if (!params.get(sub).equals("null")) {
					param = params.get(sub);
				}
				req = req.replace(element, param);
			}

			// 以下处理注释，去掉/**/
			Pattern p2 = Pattern.compile("/\\*.*?\\*/");
			Matcher mat2 = p2.matcher(req);
			List<String> elements2 = new ArrayList<String>();

			while (mat2.find()) {
				String element = mat2.group();
				elements2.add(element);
			}

			for (String element : elements2) {
				req = req.replace(element, "");
			}
		} catch(Exception e){
			throw new AngryException("请求初始化失败，请检查模板文件和数据文件是否对应",e);
		}
		
		return req;

	}

	/**
	 * 替换指定字段的请求值
	 * 
	 * @param reqBody
	 *            请求body
	 * @param column
	 *            需要替换的变量名
	 * @param value
	 *            替换后的值
	 * @return
	 */	
	public static String updateRequestBodyByColumn(String reqBody, String column, String value) {
		String element = "\\$\\{#" + column + "}";
		return reqBody.replaceAll(element, value);
	}

	
	/**
	 * 返回用于登录的64位加密串
	 * @param userName
	 * @param passWord
	 * @return
	 */
	public static String base64(String userName,String passWord){
		String base64Binary = "";
		try {
			base64Binary =  DatatypeConverter.printBase64Binary(
					((userName + ":" + passWord).getBytes("UTF-8")));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return base64Binary;
	}
	
	/**
	 * 读取配置文件(test.properties)的内容
	 * 
	 * @return
	 */
	public static Properties getProperties() {
		return getProperties(null);
	}
	
	/**
	 * 读取指定配置文件的内容
	 * 
	 * @return
	 */
	public static Properties getProperties(String filePath) {
		InputStream inputStream = null;
		if (filePath == null || "".equals(filePath))
			inputStream = FileUtil.class.getResourceAsStream(TEST_PROPERTIES);
		else
			inputStream = FileUtil.class.getResourceAsStream(filePath);
		Properties properties = new Properties();
		try {
			properties.load(inputStream);
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		return properties;
	}
	
	/**
	 * 
	 * 取得调用者所在类路径中的文件的绝对路径 如getFilePath("/xxx.txt")表示取得classes根目录下xxx.txt的绝对路径
	 * 所有放在src/main/resources目录下的资源文件都会自动复制到classes目录下
	 * 
	 * @param filePath
	 * @return
	 */
	@SuppressWarnings({ "deprecation", "restriction" })
	public static String getFilePath(String filePath) {
		try {
			return new File(sun.reflect.Reflection.getCallerClass(2)
					.getResource(filePath).toURI()).getAbsolutePath();
		} catch (URISyntaxException ex) {
			throw new RuntimeException(ex);
		} catch (NullPointerException ex) {
			logger.warn("The file main/resources" + filePath + " is not exist!");
		}
		return null;
	}
}
