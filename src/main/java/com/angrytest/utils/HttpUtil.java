package com.angrytest.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.testng.Reporter;

/**
 * httpclient工具类
 *
 * @ClassName: HttpUtil
 * @Description: httpclient工具类
 * @author AngryTester
 * @date 2018年1月25日 下午5:52:40
 *
 */
public class HttpUtil {

	private static final CloseableHttpClient httpClient;

	static {
		PoolingHttpClientConnectionManager httpClientConnectionManager = new PoolingHttpClientConnectionManager();
		httpClientConnectionManager.setMaxTotal(2000);
		httpClientConnectionManager.setDefaultMaxPerRoute(200);
		RequestConfig defaultRequestConfig = RequestConfig.custom().setSocketTimeout(60000).setConnectTimeout(5000)
				.setConnectionRequestTimeout(5000).build();
		// 设置重定向策略
		LaxRedirectStrategy redirectStrategy = new LaxRedirectStrategy();
		httpClient = HttpClients.custom().setConnectionManager(httpClientConnectionManager)
				.setDefaultRequestConfig(defaultRequestConfig).setRedirectStrategy(redirectStrategy).build();
	}

	/**
	 * 发送soap请求
	 * 
	 * @param url
	 *            wsdl地址
	 * @param soapXml
	 *            soap请求内容
	 * @return
	 */
	public static Document postSoap(String url, String soapXml) {
		HttpPost post = new HttpPost(url);
		post.setHeader("Content-Type", "application/soap+xml;charset=UTF-8");
		StringEntity data = new StringEntity(soapXml, Charset.forName("UTF-8"));
		post.setEntity(data);
		HttpResponse res;
		String resStr = "";
		Document doc = null;
		try {
			res = httpClient.execute(post);
			int statusCode = res.getStatusLine().getStatusCode();
			resStr = EntityUtils.toString(res.getEntity(), "UTF-8");
			doc = DocumentHelper.parseText(resStr);
			Reporter.log("请求soap内容为:");
			Reporter.log(soapXml);
			Reporter.log("返回响应状态码为:");
			Reporter.log(statusCode + "");
			Reporter.log("返回响应内容为:");
			Reporter.log(doc.asXML());
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		} finally {
			post.releaseConnection();
		}
		return doc;
	}

	/**
	 * 发送rest请求
	 * 
	 * @param url
	 *            接口地址
	 * @param restJson
	 *            rest请求内容
	 * @return
	 */
	public static JSONObject postRest(String url, String restJson) {
		HttpPost post = new HttpPost(url);
		StringEntity entity = new StringEntity(restJson, "utf-8");
		entity.setContentEncoding("UTF-8");
		entity.setContentType("application/json");
		post.setEntity(entity);
		HttpResponse res;
		JSONObject response = null;
		try {
			res = httpClient.execute(post);
			int statusCode = res.getStatusLine().getStatusCode();
			response = new JSONObject(
					new org.json.JSONTokener(new InputStreamReader(res.getEntity().getContent(), "UTF-8")));
			Reporter.log("请求json为:");
			Reporter.log(restJson);
			Reporter.log("返回响应状态码为:");
			Reporter.log(statusCode + "");
			Reporter.log("返回响应内容为:");
			Reporter.log(response.toString());
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (UnsupportedOperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			post.releaseConnection();
		}

		return response;
	}
	
	/**
	 * 发送rest请求
	 * 
	 * @param url
	 *            接口地址
	 * @param restJson
	 *            rest请求内容
	 * @param param
	 * 			 构造Head头
	 * @return
	 * @throws JSONException 
	 */
	public static JSONObject postRest(String url, String restJson, Map<String, String> param){
		HttpPost post = new HttpPost(url);
		StringEntity entity = new StringEntity(restJson, "utf-8");
		entity.setContentEncoding("UTF-8");
		for (Map.Entry<String, String> entry : param.entrySet()) {
			post.setHeader(entry.getKey(), entry.getValue());    
		}
		post.setEntity(entity);
		
		HttpResponse res;
		JSONObject response = null;
		try {
			res = httpClient.execute(post);
			int statusCode = res.getStatusLine().getStatusCode();
			InputStream content = res.getEntity().getContent();
			String cName = content.getClass().getName();
			if("org.apache.http.conn.EofSensorInputStream".equals(cName)){
				Reader input = new InputStreamReader(content, "UTF-8");
				JSONTokener rest = new org.json.JSONTokener(input);
				response = new JSONObject(rest );
				Reporter.log("请求json为:");
				Reporter.log(restJson);
				Reporter.log("返回响应状态码为:");
				Reporter.log(statusCode + "");
				Reporter.log("返回响应内容为:");
				Reporter.log(response.toString());
			}else if("org.apache.http.impl.io.EmptyInputStream".equals(cName)){
				response = new JSONObject("{'statusCode':"+statusCode+"}");
			}else{
				try {
					throw new Exception("请求参数错误或者未授权！！！");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (UnsupportedOperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			post.releaseConnection();
		}
		return response;
	}
	
	/**
	 * 发送rest请求put方法
	 * 
	 * @param url
	 *            接口地址
	 * @param restJson
	 *            rest请求内容
	 * @return
	 */
	public static JSONObject putRest(String url, String restJson,String auth) {
		HttpPut put = new HttpPut(url);
		StringEntity entity = new StringEntity(restJson, "utf-8");
		entity.setContentEncoding("UTF-8");
		entity.setContentType("application/json");
		put.setEntity(entity);
		
		put.setHeader("Authorization", auth);
		
		HttpResponse res;
		JSONObject response = null;
		try {
			res = httpClient.execute(put);
			int statusCode = res.getStatusLine().getStatusCode();
			InputStream content = res.getEntity().getContent();
			String cName = content.getClass().getName();
			if("org.apache.http.conn.EofSensorInputStream".equals(cName)){
				Reader input = new InputStreamReader(content, "UTF-8");
				JSONTokener rest = new org.json.JSONTokener(input);
				response = new JSONObject(rest );
				Reporter.log("请求json为:");
				Reporter.log(restJson);
				Reporter.log("返回响应状态码为:");
				Reporter.log(statusCode + "");
				Reporter.log("返回响应内容为:");
				Reporter.log(response.toString());
			}else if("org.apache.http.impl.io.EmptyInputStream".equals(cName)){
				response = new JSONObject("{'statusCode':"+statusCode+"}");
			}else{
				try {
					throw new Exception("请求参数错误或者未授权！！！");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (UnsupportedOperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			put.releaseConnection();
		}
		return response;
	}
	
	
	/**
	 * 发送rest请求delete方法
	 * 
	 * @param url
	 *            接口地址
	 * @param restJson
	 *            rest请求内容
	 * @return
	 */
	public static JSONObject deleteRest(String url, String restJson,String auth) {
		
        class HttpDeleteWithBody extends HttpEntityEnclosingRequestBase {
            public static final String METHOD_NAME = "DELETE";

            @SuppressWarnings("unused")
            public HttpDeleteWithBody() {
            }
            @SuppressWarnings("unused")
            public HttpDeleteWithBody(URI uri) {
                setURI(uri);
            }
            public HttpDeleteWithBody(String uri) {
                setURI(URI.create(uri));
            }
            public String getMethod() {
                return METHOD_NAME;
            }
        }

        HttpDeleteWithBody httpdelete = new HttpDeleteWithBody(url);
		StringEntity entity = new StringEntity(restJson, "utf-8");
		entity.setContentEncoding("UTF-8");
		entity.setContentType("application/json");
		httpdelete.setEntity(entity);
		
		httpdelete.setHeader("Authorization", auth);
		
		HttpResponse res;
		JSONObject response = null;
		try {
			res = httpClient.execute(httpdelete);
			int statusCode = res.getStatusLine().getStatusCode();
			InputStream content = res.getEntity().getContent();
			String cName = content.getClass().getName();
			if("org.apache.http.conn.EofSensorInputStream".equals(cName)){
				Reader input = new InputStreamReader(content, "UTF-8");
				JSONTokener rest = new org.json.JSONTokener(input);
				response = new JSONObject(rest );
				Reporter.log("请求json为:");
				Reporter.log(restJson);
				Reporter.log("返回响应状态码为:");
				Reporter.log(statusCode + "");
				Reporter.log("返回响应内容为:");
				Reporter.log(response.toString());
			}else if("org.apache.http.impl.io.EmptyInputStream".equals(cName)){
				response = new JSONObject("{'statusCode':"+statusCode+"}");
			}else{
				try {
					throw new Exception("请求参数错误或者未授权！！！");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (UnsupportedOperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			httpdelete.releaseConnection();
		}
		return response;
	}
}