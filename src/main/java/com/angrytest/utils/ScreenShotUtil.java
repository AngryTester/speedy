package com.angrytest.utils;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Date;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.angrytest.reportng.HTMLReporter;

/**
 * ScreenShotUtil 截图工具类
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author AngryTester
 * @date 2016年04月18日 下午7:40:11
 *
 */
public class ScreenShotUtil {

	public static String screenShotByDriver(WebDriver driver) {
		String picPath = "";
		String screenShotPath = createScreenShotPath();
		String time = DateUtil.dateToStr(new Date(), "yyyyMMdd-HHmmss");
		try {
			File source_file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE); // 关键代码，执行屏幕截图，默认会把截图保存到temp目录
			File file = new File(
					screenShotPath + File.separator + time + "-" + Thread.currentThread().getId() + ".png");
			FileUtils.copyFile(source_file, file); // 这里将截图另存到我们需要保存的目录，例如screenshot\20120406-165210.png
			picPath = file.getAbsolutePath();
			return picPath;
		} catch (Exception e) {
			return screenShotByDesktop();
		}
	}

	// 如果通过webdriver截图失败的话，就采用桌面截图，后续添加上
	public static String screenShotByDesktop() {
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		String picPath = "";
		String time = DateUtil.dateToStr(new Date(), "yyyyMMdd-HHmmss");
		String screenShotPath = createScreenShotPath();
		try {
			BufferedImage screen = (new Robot())
					.createScreenCapture(new Rectangle(0, 0, (int) d.getWidth(), (int) d.getHeight()));
			String name = screenShotPath + HTMLReporter.FILE_SEPARATOR + time + ".png";
			File file = new File(name);
			ImageIO.write(screen, "png", file);
			picPath = file.getAbsolutePath();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("截图失败！！！\n" + e.getMessage());
		}
		return picPath;
	}

	private static String createScreenShotPath() {
		String screenShotPath = HTMLReporter.REPORT_DIRECTORY_TEMP + HTMLReporter.FILE_SEPARATOR + "screenshot";
		// 这里定义了截图存放目录名
		if (!(new File(screenShotPath).isDirectory())) { // 判断是否存在该目录
			new File(screenShotPath).mkdir(); // 如果不存在则新建一个目录
		}
		return screenShotPath;
	}
}
