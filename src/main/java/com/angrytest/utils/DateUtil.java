package com.angrytest.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * 日期操作工具类
 * 
 * @ClassName: DateUtil
 * @Description: 日期操作工具类
 * @author AngryTester
 * @date 2017年5月25日 下午3:53:30
 * 
 */
public class DateUtil {

	public static String dateToStr(Date date, String pattern) {
		return dateToStr(date, pattern, Locale.CHINA);
	}

	public static String dateToStr(Date date, String pattern, Locale locale) {
		if (pattern == null) {
			pattern = "yyyy-MM-dd HH:mm:ss.SSS";
		}
		DateFormat ymdhmsFormat = new SimpleDateFormat(pattern, locale);

		return ymdhmsFormat.format(date);
	}

	public static Date strToDate(String str, String pattern) throws ParseException {
		return strToDate(str, pattern, Locale.CHINA);
	}

	public static Date strToDate(String str, String pattern, Locale locale) throws ParseException {
		if (pattern == null) {
			pattern = "yyyy-MM-dd HH:mm:ss.SSS";
		}
		DateFormat ymdhmsFormat = new SimpleDateFormat(pattern, locale);
		return ymdhmsFormat.parse(str);
	}

	public static Date getToday() {
		Calendar ca = Calendar.getInstance();
		return ca.getTime();
	}

	public static String getToday(String pattern) {
		Calendar ca = Calendar.getInstance();
		return dateToStr(ca.getTime(), pattern, Locale.CHINA);
	}

	public static Date mkDate(int year, int month, int date) {
		Calendar ca = Calendar.getInstance();
		ca.set(year, month - 1, date);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		sdf.format(ca.getTime());
		return ca.getTime();
	}

	public Date getGmtDate(Long time) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(time);
		int offset = calendar.get(Calendar.ZONE_OFFSET) / 3600000 + calendar.get(Calendar.DST_OFFSET) / 3600000;
		calendar.add(Calendar.HOUR, -offset);
		Date date = calendar.getTime();
		return date;
	}

	public static String getSpecifyDate(int interval, String format) {
		return getSpecifyDate(interval, format, Locale.CHINA);
	}

	public static String getSpecifyDate(int interval, String format, Locale locale) {

		Calendar cal = new GregorianCalendar();
		cal.add(Calendar.DATE, interval);
		return dateToStr(cal.getTime(), format, locale);
	}

	public static String getSpecifyMonth(int interval, String format) {
		return getSpecifyMonth(interval, format, Locale.CHINA);
	}

	public static String getSpecifyMonth(int interval, String format, Locale locale) {
		Calendar cal = new GregorianCalendar();
		cal.add(Calendar.MONTH, interval);
		return dateToStr(cal.getTime(), format, locale);
	}

	public static String getSpecifyYear(int interval, String format) {
		return getSpecifyYear(interval, format, Locale.CHINA);
	}

	public static String getSpecifyYear(int interval, String format, Locale locale) {
		Calendar cal = new GregorianCalendar();
		cal.add(Calendar.YEAR, interval);
		return dateToStr(cal.getTime(), format, locale);
	}

	public static String getSpecifyDate(String date, int interval, String format) {
		return getSpecifyDate(date, interval, format, Locale.CHINA);
	}

	public static String getSpecifyDate(String date, int interval, String format, Locale locale) {

		Date d = null;
		try {
			d = strToDate(date, "yyyy-MM-dd");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Calendar cal = new GregorianCalendar();
		cal.setTime(d);
		cal.add(Calendar.DATE, interval);
		return dateToStr(cal.getTime(), format, locale);
	}

	public static String getDuration(Date startDate, Date endDate) {
		long time = (endDate.getTime() - startDate.getTime()) / 1000;
		int mm = (int) time / 60; // 共计分钟数
		int hh = (int) time / 3600; // 共计小时数
		int dd = (int) hh / 24; // 共计天数
		int ss = (int) (time - mm * 60);// 余秒数
		int ms = (int) (endDate.getTime() - startDate.getTime()) % 1000;// 余毫秒数
		StringBuffer duration = new StringBuffer();
		if (dd > 0) {
			duration.append(dd + "d");
		}
		if (hh > 0) {
			duration.append(hh + "h");
		}
		if (mm > 0) {
			duration.append(mm + "m");
		}
		if (ss > 0) {
			duration.append(ss + "s");
		}
		duration.append(ms + "ms");
		return duration.toString();
	}

	public static String getDuration(long elapsed) {
		long time = elapsed / 1000;
		int mm = (int) time / 60; // 共计分钟数
		int hh = (int) time / 3600; // 共计小时数
		int dd = (int) hh / 24; // 共计天数
		int ss = (int) (time - mm * 60);// 余秒数
		int ms = (int) (time % 1000);// 余毫秒数
		StringBuffer duration = new StringBuffer();
		if (dd > 0) {
			duration.append(dd + "d");
		}
		if (hh > 0) {
			duration.append(hh + "h");
		}
		if (mm > 0) {
			duration.append(mm + "m");
		}
		duration.append(ss + "." + ms + "s");
		return duration.toString();
	}

}
