package com.angrytest.pagefactory;

import java.lang.reflect.Field;
import java.lang.reflect.Proxy;

import com.angrytest.browsers.Browser;
import com.angrytest.pagefactory.interfaces.IBody;
import com.angrytest.pagefactory.interfaces.IButton;
import com.angrytest.pagefactory.interfaces.ICell;
import com.angrytest.pagefactory.interfaces.ICheckbox;
import com.angrytest.pagefactory.interfaces.IDiv;
import com.angrytest.pagefactory.interfaces.IElement;
import com.angrytest.pagefactory.interfaces.IFileField;
import com.angrytest.pagefactory.interfaces.IForm;
import com.angrytest.pagefactory.interfaces.IFrame;
import com.angrytest.pagefactory.interfaces.IInput;
import com.angrytest.pagefactory.interfaces.ILink;
import com.angrytest.pagefactory.interfaces.IOption;
import com.angrytest.pagefactory.interfaces.IRadio;
import com.angrytest.pagefactory.interfaces.IRow;
import com.angrytest.pagefactory.interfaces.ISelect;
import com.angrytest.pagefactory.interfaces.ISpan;
import com.angrytest.pagefactory.interfaces.ITable;
import com.angrytest.pagefactory.interfaces.ITextField;

public class PO {
	public static void initPage(Object page, Browser browser) {
		Field[] fields = page.getClass().getDeclaredFields();
		for (Field field : fields) {
			LocatingHandle lh = new LocatingHandle(browser, field, fields);
			Object value = Proxy.newProxyInstance(page.getClass().getClassLoader(), new Class[] { IBody.class, IButton.class, ICell.class, ICheckbox.class, IDiv.class, IElement.class,
					IFileField.class, IForm.class, IFrame.class, IInput.class, ILink.class, IOption.class, IRadio.class, IRow.class, ISelect.class, ISpan.class, ITable.class, ITextField.class }, lh);
			if (value != null) {
				try {
					field.setAccessible(true);
					field.set(page, value);
				} catch (IllegalAccessException e) {
					throw new RuntimeException(e);
				}
			}
		}
	}
}
