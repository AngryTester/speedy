package com.angrytest.pagefactory.interfaces;

import java.util.List;
import java.util.Map;

import com.angrytest.elements.Element;


public interface ITextField {
	public Boolean exists(int timeout);
	public Boolean exists();
	public String text();
	public String tagName();
	public String className();
	public String id();
	public String outerHTML();
	public String getStyle(String name);
	public String getAttribute(String name);
	public Boolean enable();
	public Boolean enable(int timeout);
	public Boolean visible();
	public Boolean visible(int timeout);
	public Boolean invisible(int timeout);
	public Element parent(String options);
	public Element parent();
	public List<Element> children();
	public Element previous();
	public Element next();
	public String previousTextNodeValue();
	public String nextTextNodeValue();
	public List<Element> getElementList(String selectors);
	public List<String> getElementTextList(String selectors);
	public Map<String, String> getAttributes();
	public void setAttribute(String property, String value);
	public void setInnerText(String value);
	public void click();
	public void clickJs();
	public void fireEvent(String name);
	public void drag(Element element);
	public void doubleClick();
	public void scrollIntoView(Boolean top);
	public void focus();
	public void mouseClick();
	public String readonly();
	public void readonly(Boolean readonly);
	public int maxLength();
	public void maxlength(int length);
	public void sendKeys(CharSequence... value);
	public void set(String value);
	public void append(String value);
	public void clear();
}
