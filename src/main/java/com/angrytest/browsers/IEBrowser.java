package com.angrytest.browsers;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.angrytest.enums.BrowserType;
import com.angrytest.enums.RunResult;
import com.angrytest.enums.StepType;
import com.angrytest.exceptions.AngryException;
import com.angrytest.logs.LogModule;

/**
 * IE
 *
 * @ClassName: IEBrowser
 * @Description: IE
 * @author AngryTester
 * @date 2017年5月18日 上午11:15:00
 *
 */
public class IEBrowser extends Browser {
	public IEBrowser() {
		System.setProperty("webdriver.ie.driver", ("src/main/resources/IEDriverServer.exe").toString());
		DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
		capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
		setDriver(new InternetExplorerDriver(capabilities));
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		setCurrentDriver(driver);
		setCurrentBrowserType(BrowserType.IE);
		drivers.add(driver);
		Map<Long,WebDriver> map = new HashMap<Long,WebDriver>();
		map.put(Thread.currentThread().getId(), getCurrentDriver());
		drivers_map.add(map);
	}

	public static IEBrowser start(String url) {
		IEBrowser ie = new IEBrowser();
		try {
			ie.methodInfo = "IEBrowser().start(\"" + url + "\")";
			ie.driver.get(url);
			ie.driver.manage().window().maximize();
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, ie.methodInfo, RunResult.FAIL, e.getMessage(), ie.driver);
			throw new AngryException(ie.methodInfo + "操作失败!");
		}
		LogModule.logStepPass(StepType.ACTION, ie.methodInfo, RunResult.PASS, ie.driver);
		return ie;
	}
}
