package com.angrytest.browsers;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.angrytest.enums.BrowserType;
import com.angrytest.enums.DriverMode;
import com.angrytest.enums.RunResult;
import com.angrytest.enums.StepType;
import com.angrytest.exceptions.AngryException;
import com.angrytest.logs.LogModule;
import com.angrytest.utils.Config;

/**
 * 谷歌浏览器
 *
 * @ClassName: ChromeBrowser
 * @Description: 谷歌浏览器
 * @author AngryTester
 * @date 2017年5月17日 上午11:44:42
 *
 */
public class ChromeBrowser extends Browser {

	public ChromeBrowser() {
		// 根据当前操作系统选择不同驱动
		if (!System.getProperties().getProperty("os.name").contains("Windows")) {
			System.setProperty("webdriver.chrome.driver", ("src/main/resources/chromedriver").toString());
		} else {
			System.setProperty("webdriver.chrome.driver", ("src/main/resources/chromedriver.exe").toString());
		}
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		capabilities.setCapability("chrome.switches", Arrays.asList("--incognito"));
		ChromeOptions options = new ChromeOptions();
		options.addArguments("chrome.switches");
		options.addArguments("--test-type", "--ignore-certificate-errors");
		// 根据当前driver模式选择是否启用grid
		if (DriverMode.valueOf(Config.getString("driverMode").toUpperCase()) == DriverMode.LOCAL) {
			setDriver(new ChromeDriver(options));
		} else {
			try {
				setDriver(new RemoteWebDriver(new URL(Config.getString("hub")), capabilities));
			} catch (MalformedURLException e) {
				throw new AngryException("hub地址有误,格式应为http://ip:port/wd/hub");
			}
		}
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		setCurrentDriver(driver);
		setCurrentBrowserType(BrowserType.CHROME);
		drivers.add(driver);
		Map<Long,WebDriver> map = new HashMap<Long,WebDriver>();
		map.put(Thread.currentThread().getId(), getCurrentDriver());
		drivers_map.add(map);
	}

	@Override
	public String version(String browserType) {
		return super.version("Chrome");
	}

	public static ChromeBrowser start(String url) {
		ChromeBrowser cb = new ChromeBrowser();
		try {
			cb.methodInfo = "ChromeBrowser().start(\"" + url + "\")";
			if (url.indexOf("http://") >= 0 || url.indexOf("https://") >= 0) {
			} else {
				url = "http://" + url;
			}
			cb.driver.get(url);
			cb.driver.manage().window().maximize();
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, cb.methodInfo, RunResult.FAIL, e.getMessage(), cb.driver);
			throw new AngryException(cb.methodInfo + "操作失败!");
		}
		LogModule.logStepPass(StepType.ACTION, cb.methodInfo, RunResult.PASS, cb.driver);
		return cb;
	}
}
