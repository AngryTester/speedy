package com.angrytest.browsers;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.angrytest.enums.BrowserType;
import com.angrytest.enums.DriverMode;
import com.angrytest.enums.RunResult;
import com.angrytest.enums.StepType;
import com.angrytest.exceptions.AngryException;
import com.angrytest.logs.LogModule;
import com.angrytest.utils.Config;

/**
 * phantomjs
 *
 * @ClassName: GhostBrowser
 * @Description: phantomjs
 * @author AngryTester
 * @date 2017年5月18日 上午11:03:42
 *
 */
public class GhostBrowser extends Browser {
	public GhostBrowser() {
		// 根据当前操作系统选择不同驱动
		if (System.getProperties().getProperty("os.name").contains("Windows")) {
			System.setProperty("phantomjs.binary.path", ("src/main/resources/phantomjs.exe").toString());
		} else {
			System.setProperty("phantomjs.binary.path", ("src/main/resources/phantomjs").toString());
		}
		DesiredCapabilities dCaps = DesiredCapabilities.phantomjs();
		String[] phantomArgs = new String[] { "--webdriver-loglevel=NONE", " --ignore-ssl-errors=true" };
		dCaps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, phantomArgs);
		dCaps.setCapability(PhantomJSDriverService.PHANTOMJS_PAGE_SETTINGS_PREFIX + "userAgent",
				"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.97 Safari/537.11");
		dCaps.setJavascriptEnabled(true);
		dCaps.setCapability("takesScreenshot", true);
		// 根据driver模式选择是否启用grid
		if (DriverMode.valueOf(Config.getString("driverMode").toUpperCase()) == DriverMode.LOCAL) {
			setDriver(new PhantomJSDriver(dCaps));
		} else {
			try {
				setDriver(new RemoteWebDriver(new URL(Config.getString("hub")), dCaps));
			} catch (MalformedURLException e) {
				throw new AngryException("hub地址有误,格式应为http://ip:port/wd/hub");
			}
		}

		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		setCurrentDriver(driver);
		setCurrentBrowserType(BrowserType.GHOST);
		drivers.add(driver);
		Map<Long,WebDriver> map = new HashMap<Long,WebDriver>();
		map.put(Thread.currentThread().getId(), getCurrentDriver());
		drivers_map.add(map);
	}

	public static GhostBrowser start(String url) {

		GhostBrowser gb = new GhostBrowser();
		try {
			gb.methodInfo = "GhostBrowser().start(\"" + url + "\")";
			if (url.indexOf("http://") >= 0 || url.indexOf("https://") >= 0) {
			} else {
				url = "http://" + url;
			}
			gb.driver.get(url);
			gb.driver.manage().window().maximize();
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, gb.methodInfo, RunResult.FAIL, e.getMessage(), gb.driver);
			throw new AngryException(gb.methodInfo + "操作失败!");
		}
		LogModule.logStepPass(StepType.ACTION, gb.methodInfo, RunResult.PASS, gb.driver);
		return gb;
	}
}
