package com.angrytest.browsers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.angrytest.containers.Container;
import com.angrytest.enums.BrowserProcessType;
import com.angrytest.enums.BrowserType;
import com.angrytest.enums.DriverMode;
import com.angrytest.enums.RunResult;
import com.angrytest.enums.StepType;
import com.angrytest.exceptions.AngryException;
import com.angrytest.logs.LogModule;
import com.angrytest.utils.Config;
import com.angrytest.utils.FileUtil;
import com.angrytest.utils.SystemUtil;

/**
 * 浏览器
 *
 * @ClassName: Browser
 * @Description: 浏览器
 * @author AngryTester
 * @date 2017年4月11日 上午10:43:00
 *
 */
public class Browser extends Container {

	public WebDriver currentDriver;
	public BrowserType currentBrowserType = null;
	public BrowserProcessType currentBrowserProcess;

	public static List<WebDriver> drivers = new ArrayList<WebDriver>();
	public static List<Map<Long,WebDriver>> drivers_map = new ArrayList<Map<Long,WebDriver>>();
	public static int TIMEOUT = 60;

	public WebDriver getCurrentDriver() {
		return currentDriver;
	}

	public void setCurrentDriver(WebDriver currentDriver) {
		this.currentDriver = currentDriver;
	}

	public BrowserType getCurrentBrowserType() {
		return currentBrowserType;
	}

	public void setCurrentBrowserType(BrowserType currentBrowserType) {
		this.currentBrowserType = currentBrowserType;
	}

	public BrowserProcessType getCurrentBrowserProcess() {
		return currentBrowserProcess;
	}

	public void setCurrentBrowserProcess(BrowserProcessType currentBrowserProcess) {
		this.currentBrowserProcess = currentBrowserProcess;
	}

	public void killBrowserProcess() {
		killBrowserProcess(getCurrentBrowserType());
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Browser() {
		StackTraceElement stack[] = (new Throwable()).getStackTrace();
		try {
			Class calledClass = Class.forName(stack[1].getClassName());
			if (calledClass.isAssignableFrom(this.getClass())) {
				return;
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void killBrowserProcess(BrowserType browserType) {
		// 当前非windows系统或当前为grid模式直接返回
		if (!System.getProperties().getProperty("os.name").contains("Windows")
				|| DriverMode.valueOf(Config.getString("driverMode").toUpperCase()) == DriverMode.GRID)
			return;
		switch (browserType) {
		case IE:
			SystemUtil.killProcess("iexplore.exe");
			SystemUtil.killProcess("IEDriverServer.exe");
			break;
		case CHROME:
			SystemUtil.killProcess("chrome.exe");
			SystemUtil.killProcess("chromedriver.exe");
			break;
		case FIREFOX:
			SystemUtil.killProcess("firefox.exe");
			break;
		case GHOST:
			SystemUtil.killProcess("phantomjs.exe");
		default:
			break;
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void killAllBrowserProcess() {
		if (!System.getProperties().getProperty("os.name").contains("Windows")
				|| DriverMode.valueOf(Config.getString("driverMode").toUpperCase()) == DriverMode.GRID)
			return;
		SystemUtil.killProcess("IEDriverServer.exe");
		SystemUtil.killProcess("iexplore.exe");
		SystemUtil.killProcess("chromedriver.exe");
		SystemUtil.killProcess("chrome.exe");
		SystemUtil.killProcess("phantomjs.exe");
		SystemUtil.killProcess("firefox.exe");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String currentUrl() {
		String currentUrl = "";
		String methodInfo = "BaseBrowser.currentUrl()";
		try {
			currentUrl = currentDriver.getCurrentUrl();
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), currentDriver);
			throw new AngryException(methodInfo + "操作失败!");
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, currentDriver);
		return currentUrl;
	}

	public static void closeAll() {
		for (WebDriver driver : drivers) {
			try {
				driver.quit();
			} catch (Exception e) {
				// e.printStackTrace();
			}
		}
		drivers.clear();

	}
	
	/**
	 * 根据当前线程id退出driver
	 * @param id
	 */
	public static void close(Long id){
		if(!drivers_map.isEmpty()){
			for(Map<Long,WebDriver> map:drivers_map){
				if(map.containsKey(id)){
					WebDriver dr = map.get(id);
					dr.quit();
					drivers_map.remove(map);
					break;
				}
			}
		}
	}

	/**
	 * 页面定位
	 */
	@Override
	public Browser locate() {
		String title = "";
		try {
			if (hasFrame) {
				driver.switchTo().defaultContent();
				hasFrame = false;
			}
			currentDriver = driver;
			title = driver.getTitle();
		} catch (Exception e) {
			// e.printStackTrace();
		}
		methodInfo = this.getClass().getSimpleName() + "(\"" + title + "\")";
		return this;
	}

	@Override
	public Browser wd() {
		locate();
		return this;
	}

	/**
	 * 最大化当前browser页面
	 */
	public void maximizeWindow() {
		locate();
		methodInfo += ".maximizeWindow()";
		try {
			driver.manage().window().maximize();
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!");
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	/**
	 * 导航当前browser窗口到指定的位置
	 * 
	 * @param url
	 *            要导航的位置
	 */
	public void goTo(String url) {
		locate();
		methodInfo += ".goTo(\"" + url + "\")";
		if (url.indexOf("http://") >= 0 || url.indexOf("https://") >= 0) {

		} else {
			url = "http://" + url;
		}

		try {
			driver.get(url);
			driver.manage().window().maximize();
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!");
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	/**
	 * 获取当前URL
	 */
	public String url() {
		locate();
		String url = "";
		methodInfo += ".url()";
		try {
			url = driver.getCurrentUrl();
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!");
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
		return url;
	}

	/**
	 * 获取当前页面的文本内容
	 */
	public String text() {
		locate();
		String text = "";
		methodInfo += ".text()";
		try {
			text = driver.findElement(By.tagName("body")).getText();
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!");
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
		return text;
	}

	/**
	 * 获取当前页面的源码
	 */
	public String html() {
		locate();
		String html = "";
		methodInfo += ".html()";
		try {
			html = driver.getPageSource();
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!");
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
		return html;
	}

	/**
	 * 切换浏览器的窗口
	 */
	@SuppressWarnings("unused")
	private void switchToWindowByID(String id) {
		driver.switchTo().window(id);
	}

	/**
	 * 获取当前浏览器页面的句柄
	 * 
	 * @return
	 */
	public String getWindowHandle() {
		locate();
		String windowHandle = "";
		methodInfo += ".getWindowHandle()";
		try {
			windowHandle = driver.getWindowHandle();
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!");
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
		return windowHandle;
	}

	/**
	 * 刷新当前browser窗口
	 */
	public void refresh() {
		locate();
		methodInfo += ".refresh()";
		try {
			driver.navigate().refresh();
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), currentDriver);
			throw new AngryException(methodInfo + "操作失败!");
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, currentDriver);
	}

	/**
	 * 浏览器中的前进功能
	 */
	public void forward() {
		locate();
		methodInfo += ".forward()";
		try {
			driver.navigate().forward();
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!");
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	/**
	 * 浏览器中的后退功能
	 */
	public void back() {
		locate();
		methodInfo += ".back()";
		try {
			driver.navigate().back();
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!");
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	/**
	 * 退出当前browser即结束当前WebDriver进程
	 */
	public void quit() {
		locate();
		methodInfo += ".quit()";
		try {
			driver.quit();
			drivers.remove(driver);
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!");
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	/**
	 * 返回当前browser窗口的标题
	 * 
	 * @return 返回当前browser窗口的标题
	 */
	public String title() {
		locate();
		String title = "";
		methodInfo += ".title()";
		try {
			title = driver.getTitle();
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!");
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
		return title;
	}

	/**
	 * 滚动到页面最上方
	 */
	public void scrollToUp() {
		locate();
		methodInfo += ".scrollToUp()";
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollTo(0,0);");
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!");
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	/**
	 * 滚动到页面最下方
	 */
	public void scrollToDown() {
		locate();
		methodInfo += ".scrollToDown()";
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollTo(0,document.body.scrollHeight);");
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!");
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	/**
	 * 模拟键盘操作
	 * 
	 * @see Keys
	 * @param value
	 *            Keys提供的键盘操作，或者字符串;普通字符或者键盘的特有的按钮
	 * @remark 操作对象为页面上的激活控件（当前聚焦的控件位置）
	 * @Example browser.sendKeys(Keys.ENTER);browser.sendKeys(Keys.CONTROL,
	 *          "a");
	 */
	public void sendKeys(CharSequence... value) {
		locate();
		methodInfo += ".sendKeys()";
		try {
			driver.switchTo().activeElement().sendKeys(value);
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!");
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	/**
	 * 关闭当前页面
	 */
	public void close() {
		locate();
		methodInfo += ".close()";
		try {
			if (driver.getWindowHandles().size() > 1) {
				driver.close();
				driver.switchTo().window(driver.getWindowHandles().toArray()[0].toString());
			} else {
				driver.quit();
				drivers.remove(driver);
				if (drivers.size() > 0) {
					driver = drivers.get(0);
				}
			}
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!");
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	/**
	 * 获取浏览器版本
	 * 
	 * @param browserType
	 * @return
	 */
	public String version(String browserType) {
		locate();
		String version = "";
		methodInfo += ".version(" + browserType + ")";
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			String userAgent = (String) js.executeScript("return window.navigator.userAgent;");
			Pattern pattern = Pattern.compile(browserType + "\\/(\\d*)");
			Matcher matcher = pattern.matcher(userAgent);
			while (matcher.find()) {
				String v = matcher.group();
				version = v.split("\\/")[1];
			}
			version = browserType + version;
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!");
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
		return version;
	}

	/**
	 * 清理Browser的cookies缓存
	 */
	public void clearCookie() {
		locate();
		methodInfo += ".clearCookie()";
		try {
			driver.manage().deleteAllCookies();
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!");
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	/**
	 * 在指定等待时间内，标题包含该title的第index个浏览器页面是否存在
	 * 
	 * @param title
	 *            标题
	 * @param index
	 *            第几个
	 * @param timeout
	 *            最长等待时间
	 * @remark 主要用于判断是否有弹出新页面
	 * @Example browser.existByUrl("百度",2,30);//在20s内等待是否存在第二窗口的标题包含news
	 *          .baidu.com，存在则直接返回true，超时返回false
	 */
	public Boolean existByTitle(final String title, final int index, int timeout) {
		locate();
		methodInfo += ".existByTitle(\"" + title + "\"," + index + "," + timeout + ")";
		Boolean existed = false;
		try {
			existed = (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver input) {
					driver = getDriver();
					input = getDriver();
					Set<String> handles = input.getWindowHandles();
					List<String> ids = new ArrayList<String>();
					for (String handle : handles) {
						input.switchTo().window(handle);
						if (input.getTitle().contains(title)) {
							ids.add(handle);
							if (ids.size() == index) {
								currentDriver = input;
								return true;
							}
						}
					}
					return false;
				}
			});
			if (!existed) {

			} else {
				executeJs("window.blur();return window.focus();");
				driver.manage().window().maximize();
			}
		} catch (TimeoutException e) {
			return false;
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!");
		}
		return existed;
	}

	/**
	 * 在指定时间内标题包含该title的浏览器页面是否存在
	 * 
	 * @param title
	 *            标题
	 * @param timeout
	 *            最长等待时间
	 * @Example browser.existByTitle("百度",20);//在指定20秒内，标题包含百度的浏览器页面是否存在
	 */
	public Boolean existByTitle(String title, int timeout) {
		return existByTitle(title, 1, timeout);
	}

	/**
	 * 标题包含该title的浏览器页面是否存在
	 * 
	 * @param title
	 *            标题
	 * @Example browser.existByTitle("百度");//标题包含百度的浏览器页面是否存在
	 */
	public Boolean existByTitle(String title) {
		return existByTitle(title, 1, 1);
	}

	/**
	 * 在指定等待时间内，网址包含该url的第index个浏览器页面是否存在
	 * 
	 * @param url
	 *            网址
	 * @param index
	 *            第几个
	 * @param timeout
	 *            最长等待时间
	 * @remark 主要用于判断是否有弹出新页面
	 * @Example browser.existByUrl("news.baidu.com",2,30);//在20s内等待是否存在第二窗口的url包含news
	 *          .baidu.com，存在则直接返回true，超时返回false
	 */
	public Boolean existByUrl(final String url, final int index, int timeout) {
		locate();
		methodInfo += ".existByUrl(\"" + url + "\"," + index + "," + timeout + ")";
		Boolean existed = false;
		try {
			existed = (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver input) {
					driver = getDriver();
					input = getDriver();
					Set<String> handles = input.getWindowHandles();
					List<String> ids = new ArrayList<String>();
					for (String handle : handles) {
						input.switchTo().window(handle);
						if (input.getCurrentUrl().contains(url)) {
							ids.add(handle);
							if (ids.size() == index) {
								currentDriver = input;
								return true;
							}
						}
					}
					return false;
				}
			});
			if (!existed) {
				// LogModule.logStepFail(StepType.ACTION, methodInfo,
				// RunResult.FAIL, "找不到指定的浏览器页面", driver);
			} else {
				executeJs("window.blur();return window.focus();");
				driver.manage().window().maximize();
			}
		} catch (TimeoutException e) {
			return false;
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!");
		}
		return existed;
	}

	/**
	 * 在指定时间内网址包含该url的浏览器页面是否存在
	 * 
	 * @param url
	 *            网址
	 * @param timeout
	 *            最长等待时间
	 * @Example browser.existByUrl("news.baidu.com",20);//在指定时间内，网址包含news.baidu
	 *          .com的浏览器页面是否存在
	 */
	public Boolean existByUrl(String url, int timeout) {
		return existByUrl(url, 1, timeout);
	}

	/**
	 * 网址包含该url的浏览器页面是否存在
	 * 
	 * @param url
	 *            网址
	 * @Example browser.existByUrl("news.baidu.com");//网址包含news.baidu
	 *          .com的浏览器页面是否存在
	 */
	public Boolean existByUrl(String url) {
		return existByUrl(url, 1, 1);
	}

	/**
	 * 定位到网址包含该url的第index个浏览器页面,暂时只支持一个driver中attach
	 * 
	 * @param url
	 *            ，网址
	 * @param index
	 *            ，第几个
	 * @remark 主要针对弹出新页面时，需要定位到该新页面，默认最大等待时间为60s
	 * @Example browser.attachByUrl("http://news.baidu.com/",2);//定位到第二个网址包含baidu
	 *          .com的浏览器页面
	 */
	public void attachByUrl(final String url, final int index) {
		locate();
		methodInfo += ".attachByUrl(\"" + url + "\"," + index + ")";
		Boolean attached = false;
		try {
			attached = (new WebDriverWait(driver, TIMEOUT)).until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver input) {
					driver = getDriver();
					input = getDriver();
					Set<String> handles = input.getWindowHandles();
					List<String> ids = new ArrayList<String>();
					for (String handle : handles) {
						input.switchTo().window(handle);
						if (input.getCurrentUrl().contains(url)) {
							ids.add(handle);
							if (ids.size() == index) {
								currentDriver = input;
								return true;
							}
						}
					}
					return false;
				}
			});
			// 后面加上找不到情况
			// if (ids.size() < index) {
			if (!attached) {
				LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, "找不到指定的浏览器页面", driver);
			} else {
				executeJs("window.blur();return window.focus();");
				driver.manage().window().maximize();
			}
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!");
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	/**
	 * 定位到网址包含该url的浏览器页面
	 * 
	 * @param url
	 *            ，网址
	 * @Example browser.attachByUrl("http://news.baidu.com/");//定位到网址包含news.baidu
	 *          .com的浏览器页面
	 */
	public void attachByUrl(String url) {
		attachByUrl(url, 1);
	}

	/**
	 * 定位到标题包含该titile的第index个浏览器页面，后续可以在该页面上做操作
	 * 
	 * @param url
	 *            ，网址
	 * @param index
	 *            ，第几个
	 * @remark 主要针对弹出新页面时，需要定位到该新页面，默认最大等待时间为60s
	 * @Example browser.attachByUrl("baidu.com",2);//定位到第二个网址包含baidu.com的浏览器页面
	 */
	public void attachByTitle(final String title, final int index) {
		locate();
		methodInfo += ".attachByTitle(\"" + title + "\"," + index + ")";
		Boolean attached = false;
		try {
			attached = (new WebDriverWait(driver, TIMEOUT)).until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver input) {
					driver = getDriver();
					input = getDriver();
					Set<String> handles = input.getWindowHandles();
					List<String> ids = new ArrayList<String>();
					for (String handle : handles) {
						input.switchTo().window(handle);
						if (input.getTitle().contains(title)) {
							ids.add(handle);
							if (ids.size() == index) {
								currentDriver = input;
								return true;
							}
						}
					}
					return false;
				}
			});
			// 后面加上找不到情况
			// if (ids.size() < index) {
			if (!attached) {
				LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, "找不到指定的浏览器页面", driver);
			} else {
				executeJs("return window.focus();");
				driver.manage().window().maximize();
			}
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!");
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	/**
	 * 定位到标题包含该title的浏览器页面
	 * 
	 * @param title
	 *            ，浏览器页面标题
	 * @remark 主要针对弹出新页面时，需要定位到该新页面，默认最大等待时间为60s
	 */
	public void attachByTitle(String title) {
		attachByTitle(title, 1);
	}

	/**
	 * 自动连接到包含指定id的浏览器页面，后续可以在该页面上做操作
	 * 
	 * @param id
	 *            窗口句柄
	 * @Example browser.attachById("345656");
	 */
	public void attachById(String id) {
		locate();
		methodInfo += ".attachById(\"" + id + "\")";
		try {
			driver.switchTo().window(id);
			currentDriver = driver;
			driver.manage().window().maximize();
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, e.getMessage(), driver);
			throw new AngryException(methodInfo + "操作失败!");
		}
		LogModule.logStepPass(StepType.ACTION, methodInfo, RunResult.PASS, driver);
	}

	/**
	 * 当前browser页面的弹出对话框
	 * 
	 * @return 返回当前browser页面的弹出对话框对象
	 */
	public Alert alert() {
		Boolean exist = hasAlert(TIMEOUT);
		String methodInfo = "弹出框";
		if (exist) {
			Alert alert = driver.switchTo().alert();
			String text = alert.getText();
			LogModule.logStepPass(StepType.ACTION, methodInfo + ":" + text, RunResult.PASS);
			return alert;
		} else {
			LogModule.logStepFail(StepType.ACTION, methodInfo, RunResult.FAIL, "不存在", driver);
			throw new AngryException(methodInfo + "定位失败!");
		}
	}

	protected void fileupload(String title, String filePath) {
		try {
			Runtime rt = Runtime.getRuntime();
			rt.exec(FileUtil.getFilePath("/downloadfile.exe ") + title + " " + filePath);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected void savefile(String title, String filePath) {

		try {
			Runtime rt = Runtime.getRuntime();
			rt.exec(FileUtil.getFilePath("/savefile.exe ") + title + " " + filePath);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 等待
	 * 
	 * @param s
	 *            秒为单位
	 */
	public void sleep(int s) {
		try {
			Thread.sleep(1000 * s);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 当前browser页面的是否有弹出对话框
	 * 
	 * @param driver
	 * @return boolean
	 */
	public boolean hasAlert() {
		try {
			driver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException e) {
			return false;
		}
	}

	/**
	 * 当前browser页面的是否有弹出对话框
	 * 
	 * @param timeout
	 *            超时时间
	 * @return boolean
	 */
	public boolean hasAlert(int timeout) {
		Boolean exist = false;
		exist = (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver input) {
				return hasAlert();
			}
		});
		return exist;
	}

}
