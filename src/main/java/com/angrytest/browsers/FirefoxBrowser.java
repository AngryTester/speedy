package com.angrytest.browsers;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.angrytest.enums.BrowserType;
import com.angrytest.enums.DriverMode;
import com.angrytest.enums.RunResult;
import com.angrytest.enums.StepType;
import com.angrytest.exceptions.AngryException;
import com.angrytest.logs.LogModule;
import com.angrytest.utils.Config;
import com.sun.jna.platform.win32.Advapi32Util;
import com.sun.jna.platform.win32.WinReg;

/**
 * 火狐浏览器
 *
 * @ClassName: FirefoxBrowser
 * @Description: 火狐浏览器
 * @author AngryTester
 * @date 2017年5月17日 上午12:00:42
 *
 */
public class FirefoxBrowser extends Browser {

	public static final String FIREFOX_REGISTRY_KEY = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\firefox.exe";

	public FirefoxBrowser() {
		// 根据当前操作系统选择不同驱动
		if (!System.getProperties().getProperty("os.name").contains("Windows")) {
			System.setProperty("webdriver.gecko.driver", ("src/main/resources/geckodriver").toString());
		} else {
			System.setProperty("webdriver.gecko.driver", ("src/main/resources/geckodriver.exe").toString());
		}
		System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "/dev/null");
		FirefoxOptions options = new FirefoxOptions();
		options.setLogLevel(Level.OFF);
		DesiredCapabilities capabilities = DesiredCapabilities.firefox();
		capabilities.setCapability("moz:firefoxOptions", options);
		// 根据当前driver模式选择是否启用grid
		if (DriverMode.valueOf(Config.getString("driverMode").toUpperCase()) == DriverMode.LOCAL) {
			setDriver(new FirefoxDriver(capabilities));
		} else {
			try {
				setDriver(new RemoteWebDriver(new URL(Config.getString("hub")), capabilities));
			} catch (MalformedURLException e) {
				throw new AngryException("hub地址有误,格式应为http://ip:port/wd/hub");
			}
		}
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		setCurrentDriver(driver);
		setCurrentBrowserType(BrowserType.FIREFOX);
		drivers.add(currentDriver);
		Map<Long, WebDriver> map = new HashMap<Long, WebDriver>();
		map.put(Thread.currentThread().getId(), getCurrentDriver());
		drivers_map.add(map);
	}

	/**
	 * 获取firefox的exe路径
	 * 
	 * @return
	 */
	public static String getFirefoxInstallFolder() {
		String path = null;
		try {
			path = Advapi32Util.registryGetStringValue(WinReg.HKEY_LOCAL_MACHINE, FIREFOX_REGISTRY_KEY, "Path");
		} catch (Throwable e) {
		}
		return path;
	}

	@Override
	public String version(String browserType) {
		return super.version("Firefox");
	}

	public static FirefoxBrowser start(String url) {
		FirefoxBrowser ff = new FirefoxBrowser();
		try {
			// String title = currentDriver.getTitle();
			ff.methodInfo = "FirefoxBrowser().start(\"" + url + "\")";
			if (url.indexOf("http://") >= 0 || url.indexOf("https://") >= 0) {

			} else {
				url = "http://" + url;
			}
			ff.driver.get(url);
			ff.driver.manage().window().maximize();
		} catch (Exception e) {
			LogModule.logStepFail(StepType.ACTION, ff.methodInfo, RunResult.FAIL, e.getMessage(), ff.driver);
			throw new AngryException(ff.methodInfo + "操作失败!");
		}
		LogModule.logStepPass(StepType.ACTION, ff.methodInfo, RunResult.PASS, ff.driver);
		return ff;
	}
}
