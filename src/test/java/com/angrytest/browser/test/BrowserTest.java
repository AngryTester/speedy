package com.angrytest.browser.test;

import com.angrytest.annotations.CaseInfo;
import com.angrytest.browsers.ChromeBrowser;
import com.angrytest.browsers.FirefoxBrowser;
import com.angrytest.browsers.GhostBrowser;
import com.angrytest.pagefactory.PO;
import org.testng.annotations.Test;

import com.angrytest.assertion.Asserter;
import com.angrytest.browsers.Browser;

public class BrowserTest {
	
	private Browser browser;

	@Test
	@CaseInfo(caseName = "谷歌测试",caseDesc = "登录失败验证提示信息")
	public void chromeTest(){
		browser = ChromeBrowser.start("https://gitlab.com/users/sign_in");
		browser.input("id=>user_login").sendKeys("thx_phila@yahoo.com");
		browser.input("id=>user_password").encrypSet("123456");
		browser.button("name=>commit").click();
		Asserter asserter = new Asserter(browser);
		asserter.assertEquals(browser.div("class=>flash-alert").text(),"Invalid Login or password.");
		Asserter.assertAll();
		browser.quit();
	}

	@Test
	@CaseInfo(caseName = "火狐测试",caseDesc = "登录失败验证提示信息")
	public void firefoxTest(){
		browser = FirefoxBrowser.start("https://gitlab.com/users/sign_in");
		browser.input("id=>user_login").sendKeys("thx_phila@yahoo.com");
		browser.input("id=>user_password").encrypSet("123456");
		browser.button("name=>commit").click();
		Asserter asserter = new Asserter(browser);
		asserter.assertEquals(browser.div("class=>flash-alert").text(),"Invalid Login or password.");
		Asserter.assertAll();
		browser.quit();
	}

	@Test
	@CaseInfo(caseName = "ghost测试",caseDesc = "登录失败验证提示信息")
	public void ghostTest(){
		browser = GhostBrowser.start("https://gitlab.com/users/sign_in");
		browser.input("id=>user_login").sendKeys("thx_phila@yahoo.com");
		browser.input("id=>user_password").encrypSet("123456");
		browser.button("name=>commit").click();
		Asserter asserter = new Asserter(browser);
		asserter.assertEquals(browser.div("class=>flash-alert").text(),"Invalid Login or password.");
		Asserter.assertAll();
		browser.quit();
	}


    @Test
    @CaseInfo(caseName = "PO模式测试",caseDesc = "登录失败验证提示信息")
    public void POTest(){
        browser = GhostBrowser.start("https://gitlab.com/users/sign_in");
        LoginPage login = new LoginPage();
        PO.initPage(login,browser);
        login.userName.sendKeys("thx_phila@yahoo.com");
        login.passWord.sendKeys("123456");
        login.loginBtn.click();
        Asserter asserter = new Asserter(browser);
        asserter.assertEquals(login.msg.text(),"Invalid Login or password.");
        Asserter.assertAll();
        browser.quit();
    }




}