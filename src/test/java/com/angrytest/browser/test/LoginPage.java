package com.angrytest.browser.test;

import com.angrytest.pagefactory.annotations.Find;
import com.angrytest.pagefactory.interfaces.IButton;
import com.angrytest.pagefactory.interfaces.IDiv;
import com.angrytest.pagefactory.interfaces.IInput;

public class LoginPage {

    @Find("id=>user_login")
    public IInput userName;

    @Find("id=>user_password")
    public IInput passWord;

    @Find("name=>commit")
    public IButton loginBtn;

    @Find("class=>flash-alert")
    public IDiv msg;
}
