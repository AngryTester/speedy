package com.angrytest.restassured.test;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.Map;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.angrytest.annotations.DataSource;
import com.angrytest.utils.Config;
import com.angrytest.utils.FileUtil;
import com.github.dreamhead.moco.HttpServer;
import com.github.dreamhead.moco.RequestMatcher;
import com.github.dreamhead.moco.RestServer;
import com.github.dreamhead.moco.Runner;
import com.github.dreamhead.moco.matcher.EqRequestMatcher;
import com.github.dreamhead.moco.resource.Resource;
import com.github.dreamhead.moco.util.Jsons;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

import static com.github.dreamhead.moco.Moco.log;
import static com.github.dreamhead.moco.MocoRest.post;
import static com.github.dreamhead.moco.Moco.status;
import static com.github.dreamhead.moco.Runner.runner;
import static com.github.dreamhead.moco.Moco.eq;
import static com.github.dreamhead.moco.Moco.json;
import static com.github.dreamhead.moco.Moco.by;
import static com.github.dreamhead.moco.Moco.file;
import static com.github.dreamhead.moco.Moco.toJson;
import static com.github.dreamhead.moco.Moco.jsonPath;
import static com.github.dreamhead.moco.Moco.header;
import static com.github.dreamhead.moco.Moco.contain;
import static org.hamcrest.Matchers.is;

import static com.github.dreamhead.moco.MocoRest.restServer;

import com.google.common.net.HttpHeaders;

@SuppressWarnings("unused")
public class RestPostTest {

	private Runner runner;

	@BeforeClass
	public void setup() {

		RestAssured.baseURI = Config.getString("url");
		RestAssured.port = Config.getInt("port");
		RestAssured.basePath = "/targets";

		RestServer server = restServer(Config.getInt("port"), log());

		final Req req_success = new Req();
		req_success.userName = "admin";
		req_success.passWord = "111111";

		final Res res_success = new Res();
		res_success.code = 1;
		res_success.message = "success";

		final Req req_fail = new Req();
		req_fail.userName = "admin";
		req_fail.passWord = "123456";

		final Res res_fail = new Res();
		res_fail.code = 0;
		res_fail.message = "fails";

		server.resource("targets", post().request(json(req_success)).response(status(200), toJson(res_success)),
				post().request(json(req_fail)).response(status(200), toJson(res_fail)));
		runner = runner(server);
		runner.start();

	}

	@Test
	@DataSource(temp = "req.json", file = "req.xls")
	public void testPost(String body, Map<String, String> param) {
		given().
				contentType(ContentType.JSON).
				body(body).
		when().
				post().
		then().
				statusCode(200).
				body("code", is(Integer.parseInt(param.get("预期code")))).
				body("message", is(param.get("预期message")));
	}

	private static class Res {
		public int code;
		public String message;
	}

	private static class Req {
		public String userName;
		public String passWord;
	}

	@AfterClass
	public void teardown() {
		runner.stop();
	}

}
