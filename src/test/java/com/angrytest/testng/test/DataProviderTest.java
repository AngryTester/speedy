package com.angrytest.testng.test;


import com.angrytest.annotations.DataSource;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Map;

public class DataProviderTest {

	@Test
	@DataSource(temp="req.json",file = "req.xls")
	public void verifyData1(String body,Map<String,String> param) {
		Assert.assertTrue(body!=null);
	}
	
	@Test
	@DataSource(file = "req.xls")
	public void verifyData2(Map<String,String> param) {
		Assert.assertTrue(param!=null);
	}

}
