package com.angrytest.httpclient.test;

import com.angrytest.annotations.DataSource;
import com.angrytest.utils.Config;
import com.angrytest.utils.HttpUtil;
import com.github.dreamhead.moco.RestServer;
import com.github.dreamhead.moco.Runner;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Map;

import static com.github.dreamhead.moco.Moco.*;
import static com.github.dreamhead.moco.MocoRest.post;
import static com.github.dreamhead.moco.MocoRest.restServer;
import static com.github.dreamhead.moco.Runner.runner;

public class HttpPostRest {
	
	private Runner runner;

	@BeforeClass
	public void setup() {

		RestServer server = restServer(Config.getInt("port"), log());

		final HttpPostRest.Req req_success = new HttpPostRest.Req();
		req_success.userName = "admin";
		req_success.passWord = "111111";

		final HttpPostRest.Res res_success = new HttpPostRest.Res();
		res_success.code = 1;
		res_success.message = "success";

		final HttpPostRest.Req req_fail = new HttpPostRest.Req();
		req_fail.userName = "admin";
		req_fail.passWord = "123456";

		final HttpPostRest.Res res_fail = new HttpPostRest.Res();
		res_fail.code = 0;
		res_fail.message = "fails";

		server.resource("targets", post().request(json(req_success)).response(status(200), toJson(res_success)),
				post().request(json(req_fail)).response(status(200), toJson(res_fail)));
		runner = runner(server);
		runner.start();

	}
	
	@Test
	@DataSource(temp = "req.json", file = "req.xls")
	public void testPostRest(String request, Map<String, String> param) throws JSONException{
		JSONObject obj = HttpUtil.postRest("http://localhost:12306/targets", request);
		Assert.assertEquals(obj.getInt("code"),Integer.parseInt(param.get("预期code")));
		Assert.assertEquals(obj.getString("message"),param.get("预期message"));
	}

	private static class Res {
		public int code;
		public String message;
	}

	private static class Req {
		public String userName;
		public String passWord;
	}

	@AfterClass
	public void teardown() {
		runner.stop();
	}
}
