package com.angrytest.typesafe.config.test;

import org.testng.annotations.Test;
import org.testng.Assert;

import com.angrytest.annotations.CaseInfo;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

public class TypesafeConfigTest {

	@Test
	@CaseInfo(caseName = "解析配置文件", caseDesc = "typesafe")
	public void loadPropTest() {
		Config config = ConfigFactory.load("test");
		Assert.assertEquals("speedy", config.getString("project"));
	}

}
