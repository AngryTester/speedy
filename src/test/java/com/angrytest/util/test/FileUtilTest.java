package com.angrytest.util.test;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.angrytest.annotations.CaseInfo;
import com.angrytest.utils.FileUtil;

public class FileUtilTest {

	public static final String file = "src/test/resources/temp.json";

	/**
	 * 测试文件读取
	 */
	@Test
	@CaseInfo(caseName  = "测试文件读取", caseDesc = "返回String")
	public void testReadFile() {
		String fileContent = FileUtil.readFile(file);
		Assert.assertTrue(fileContent.contains("13912345678"));
	}

	/**
	 * 测试根据模板和输入初始化请求body
	 */
	@Test
	@CaseInfo(caseName = "测试根据模板和输入初始化请求body", caseDesc = "根据指定标识替换字段")
	public void testInitRequestBody() {
		Map<String, String> param = new HashMap<String, String>();
		param.put("contactName", "测试人员");
		String reqBody = FileUtil.initRequestBody(file, param);
		Assert.assertTrue(reqBody.contains("测试人员"));
		Assert.assertFalse(reqBody.contains("##contactName"));
	}

	/**
	 * 测试更新请求body
	 */
	@Test
	@CaseInfo(caseName = "测试更新请求body", caseDesc = "替换请求字段值")
	public void testUpdateRequestBodyByColumn() {
		String reqBody = FileUtil.readFile(file);
		String updatedBody = FileUtil.updateRequestBodyByColumn(reqBody, "email", "thx_phila@yahoo.com");
		Assert.assertTrue(updatedBody.contains("thx_phila@yahoo.com"));
		Assert.assertFalse(updatedBody.contains("#email"));
	}

}
