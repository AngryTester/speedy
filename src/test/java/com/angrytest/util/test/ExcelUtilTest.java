package com.angrytest.util.test;

import java.util.List;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.angrytest.annotations.CaseInfo;
import com.angrytest.utils.ExcelUtil;

public class ExcelUtilTest {
	
	/**
	 * 测试解析excel
	 */
	@Test
	@CaseInfo(caseName = "解析excel测试", caseDesc = "数组长度为2")
	public void excelDatasTest(){
		ExcelUtil excel = new ExcelUtil();
		List<Map<String,String>> datas = excel.excelDatas("src/test/resources/req.xls", "sheet1");
		Assert.assertEquals(datas.size(), 2,"数组长度为2");
	}
	
}
